//
//  AppDelegate.swift
//  PetsKFU
//
//  Created by Mostafa on 1/1/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManager

public let BASE_URL1 = "http://37.224.16.172:90/KFUWebServices.asmx"
public let BASE_URL2 = "http://37.224.16.172:96/api/vet"
public let BASE_URL =  "http://37.224.16.172:90"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MOLHResetable, UNUserNotificationCenterDelegate
{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        MOLH.shared.activate(true)
        IQKeyboardManager.shared().isEnabled = true
        
        let cancelButton = UIButton.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        cancelButton.setTitle("X", for: .normal)
//
    
//        UINavigationBar.appearance().backgroundColor = UIColor.appGreen
        UINavigationBar.appearance().barTintColor = UIColor.appGreen
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
//        UINavigationBar.appearance().isTranslucent = true
        
        // Local notification
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        let options: UNAuthorizationOptions = [.alert, .sound]
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted { print("User didn't permit Local Notification") }
        }
         
        return true
    }

    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = storyboard.instantiateViewController(withIdentifier: "rootnav")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert,.sound])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


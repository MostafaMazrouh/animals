//
//  UIExtentions.swift
//  PetsKFU
//
//  Created by Mostafa on 1/1/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

extension UIView
{
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue; layer.masksToBounds = newValue > 0 }
    }
    
    @IBInspectable var borderColor: UIColor {
        get { return UIColor(cgColor: layer.borderColor!) }
        set { layer.borderColor = newValue.cgColor }
    }
    
    @IBInspectable var borderWidth : CGFloat {
        set { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }
    
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }

}

class FitButton : UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

extension String {
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
}

extension UIButton
{
    func template(imageName: String, tintColor: UIColor)
    {
        let image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.tintColor = tintColor
    }
}

extension UIColor {
    static var appGreen: UIColor {get{ return UIColor(hex: "#1E93C8", alpha: 1) }}
    
    static var appOrange: UIColor {get{ return UIColor(hex: "#F4C760", alpha: 1) }}
    
//    static var appOrange: UIColor {get{ return UIColor(hex: "#ff6138", alpha: 1) }}
    //#1E93C8
//    static var appGreen: UIColor {get{ return UIColor(hex: "#00a388", alpha: 1) }}
    
    convenience init(hex:String, alpha: Int = 1)
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) { cString.remove(at: cString.startIndex) }
        
        if ((cString.count) != 6) { self.init(white: 0, alpha: 1); return }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
}

extension UIViewController
{
    func addBackgroundImage()
    {
        let imageView = UIImageView(image: UIImage(named:"bg"))
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .green
        
        view.addSubview(imageView)
        view.sendSubview(toBack: imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    }
    
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }

}

extension String
{
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}

class Constants
{
    static var isAR: Bool { return (MOLHLanguage.currentAppleLanguage() == "ar") }
}


















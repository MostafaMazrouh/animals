//
//  File.swift
//  SaryGram
//
//  Created by Mostafa on 12/20/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import Foundation

class Binder<T>
{
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T { didSet { listener?(value) } }
    
    init(_ value: T) { self.value = value }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}








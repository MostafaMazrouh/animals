//
//  DataModel.swift
//  SaryGram
//
//  Created by Mostafa on 12/20/18.
//  Copyright © 2018 Nasaq. All rights reserved.
//

import UIKit

class CodableRESTModel
{
    // The strategy to use for decoding keys. Defaults to `.useDefaultKeys`.
    // OR:: convertFromSnakeCase
    private var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy
    
    init(decodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys)
    {
        keyDecodingStrategy = decodingStrategy
    } 
}


// MARK: - Upper Layer

extension CodableRESTModel
{
    func downloadImage(url: String, handler: @escaping (UIImage?, Error?) -> Void )
    {
        self.sendRequest(urlString: url) { (sessionData, error) in
            guard error == nil, let data = sessionData else { return handler(nil, error) }
            
            guard let image = UIImage(data: data) else {return  handler(nil, CodableRESTError.imageError) }
            
            handler(image, nil)
        }
    }
    
    func getData<T: Codable>(url: String, receivedType: T.Type, isPost: Bool = true, handler: @escaping (T?, Error?) -> Void)
    {
        let httpMethod: HTTPMethod = isPost ? .post : .get
        
        // Send a request from lower leve;
        self.sendRequest(urlString: url, httpMethod: httpMethod) { [weak self] (sessionData, error) in
            guard error == nil, let data = sessionData else { return handler(nil, error) }
            
            print("sessionData: \(data)")
            
            // Prepare the comming data
            guard let jsonData = self?.decode(toType: receivedType, from: data) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(jsonData, nil)
        }
    }
    
    func postData(url: String, httpBody: [String:Any], removeGarbage: Bool = false, handler: @escaping (Data?, Error?)->Void)
    {
        self.sendRequest(urlString: url, httpMethod: .post, httpBody: httpBody) { (sessionData, error) in
            guard error == nil, let sessionData = sessionData else { return handler(nil, error) }
            
            var data: Data?
            if removeGarbage {
                data = self.removeGarbage(garbageData: sessionData)
            } else { data =  sessionData}
            
            handler(data, nil)
        }
    }
    
    func removeGarbage(garbageData: Data) -> Data?
    {
        guard let garbageString = String(data: garbageData, encoding: String.Encoding.utf8) as String? else { return nil }
        
        print("garbageString::: \(garbageString))\n")
        
        // {\"d\":null})
        let goodString = garbageString.replacingOccurrences(of: "{\"d\":null}", with: "")
        print("goodString::: \(goodString))\n")
        
        return goodString.data(using: String.Encoding.utf8)
    }
}

// MARK: - Lower Layer

extension CodableRESTModel
{
    private func sendRequest(urlString: String,
                     httpMethod: HTTPMethod = .get,
                     httpBody: [String:Any]? = nil,
                     handler: @escaping (Data?, Error?)->Void)
    {
        guard let url = URL(string: urlString)
            else { return handler(nil, CodableRESTError.urlError) }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        if httpMethod == .post
        {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let body = try! JSONSerialization.data(withJSONObject: httpBody ?? [], options: [])
            print("body: \(body)")
            request.httpBody = body
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (sessionData, response, error) in
            
            guard error == nil else { handler(nil, error); return }
            guard (response as? HTTPURLResponse)?.statusCode == 200 else { handler(nil, CodableRESTError.responseError); return }
            guard let mySessionData = sessionData else { handler(nil, CodableRESTError.sessionDataError); return }
            
            handler(mySessionData, nil)
        }
        task.resume()
    }
}

// MARK: - Codable

extension CodableRESTModel
{
    func decode<T: Codable>(toType myType: T.Type, from jsonData: Data) -> T?
    {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = self.keyDecodingStrategy
            
            let decodedData: T = try decoder.decode(myType, from: jsonData)
            return decodedData
        } catch let error {
            print("json decode error", error)
        }
        return nil
        
    }
    
    func encode<T: Codable>(fromCodable codable: T) -> Data?
    {
        do {
            let encoder = JSONEncoder()
            let encodedData = try encoder.encode(codable)
            return encodedData
        } catch let error {
            print("json encode error", error)
        }
        return nil
    }
}

enum CodableRESTError: Error
{
    case urlError
    case responseError
    case sessionDataError
    case decodingError
    case encodingError
    case imageError
}

enum HTTPMethod: String
{
    case get = "Get"
    case post = "POST"
}








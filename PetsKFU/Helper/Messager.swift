//
//  ErrorViewer.swift
//  PetsKFU
//
//  Created by Mostafa on 1/11/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class Messager
{
    static func show(message: String? = "ConnectionError".localized())
    {
        DispatchQueue.main.async
        {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default, handler: nil))
            
            present(alert: alert)
        }
    }
    
    private static func present(alert: UIAlertController)
    {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
        var upperVC = rootVC
        
        if  let navVC = rootVC as? UINavigationController,
            let visibleVC = navVC.visibleViewController
        {
            upperVC = visibleVC
        }
        else if  let tabVC = rootVC as? UITabBarController,
                 let selectedVC = tabVC.selectedViewController
        {
            upperVC = selectedVC
        }
        else if let presentedViewController = rootVC.presentedViewController
        {
            upperVC = presentedViewController
        }
        
        upperVC.present(alert, animated: true, completion: nil)
    }
}


















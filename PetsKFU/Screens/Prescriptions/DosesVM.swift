//
//  DosesVM.swift
//  PetsKFU
//
//  Created by Mostafa on 6/3/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation
import UserNotifications


class DosesVM
{
    private let crModel = CodableRESTModel()
    
    func saveDoseToServer(strPrescID: String,
                  strMedItemID: String,
                  doseDate: String,
                  doseTime: String,
                  strOwnerID: String,
                  handler: @escaping (Bool?, Error?)->())
    {
        
        let url = BASE_URL + "/SaveDoseTime"
        let httpBody = [
            "strPrescID": strPrescID,
            "strMedItemID": strMedItemID,
            "doseDate": doseDate,
            "doseTime": doseTime,
            "strOwnerID": strOwnerID
        ]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil,
                let data = data,
                let stringInt = String.init(data: data, encoding: String.Encoding.utf8),
            let int = Int.init(stringInt),
            int == 0
            else { return handler(nil, error) }
            
            handler(true, nil)
        }
    }
    
    func getOwnerDose(strOwnerID: String, handler: @escaping ([Dose]?, Error?)->())
    {
        let url = BASE_URL + "/GetDoseTimeByOwner"
        let httpBody = ["strOwnerID": strOwnerID]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let ownerDose = self?.crModel.decode(toType: [Dose].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(ownerDose, nil)
        }
    }
    
    func getNotificationsOnServer()
    {
        guard let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId") else
        {
            return
        }
        
        NotifiyVM.shared.removeAllNotifications()
        
        self.getOwnerDose(strOwnerID: strOwnerID) { [weak self] (doseArray, error) in
            
            guard let doseArray = doseArray else
            {return Messager.show(message: "Wrong".localized())}
            
            self?.saveDoses(doseArray: doseArray)
        }
    }
    
    func saveDoses(doseArray: [Dose])
    {
        print("doseArray: \(doseArray)")
        
        for dose in doseArray {
            NotifiyVM.shared.scheduleDose(dose: dose)
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
//            let center = UNUserNotificationCenter.current()
//            center.getPendingNotificationRequests { (requestArray) in
//                
//                var i = 0
//                for request in requestArray
//                {
//                    print("request: \(i+=1) : \(request)")
//                }
//            }
//        }
        
        
    }
}



























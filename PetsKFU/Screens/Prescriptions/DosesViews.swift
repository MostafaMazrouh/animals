//
//  DosesViews.swift
//  PetsKFU
//
//  Created by Mostafa on 1/20/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation
import UserNotifications
import JTAppleCalendar

class DoseCell: UITableViewCell
{
    @IBOutlet weak var medicineLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var doseNumberLabel: UILabel!
    @IBOutlet weak var doseButton: UIButton!
    
    var dose: Dose?
    var nextDose: String?
    weak var vc: DosesVC?
    
    var nextDoseDate: Date? {
        didSet { DispatchQueue.main.async {
//            self.vc?.tableView.reloadData()
            
            } }
    }
    
    func setUp(dose: Dose? = nil)
    {
        self.dose = dose
        
        doseButton.titleLabel?.numberOfLines = 0
        doseButton.titleLabel?.lineBreakMode = .byWordWrapping
        doseButton.sizeToFit()
        
        var color = UIColor.appOrange
        if let dose = dose
        {
            color = .appGreen
            medicineLabel.text = dose.Medicine_title
            quantityLabel.text = dose.Medicine_desc
            let medicineDose = dose.Medicine_dose_ar
            doseNumberLabel.text = "\("Every".localized()) \(medicineDose ?? "") \("Hour".localized())"
            contentView.backgroundColor = .clear
            
            // [2]
            self.nextDoseButton()
        } else {
            medicineLabel.text = "Medicine".localized()
            quantityLabel.text = "Quantity".localized()
            doseNumberLabel.text = "Dose".localized()
            contentView.backgroundColor = .white
            
            // [1]
            doseButton.setTitle("NextDose".localized(), for: .normal)
            doseButton.setTitleColor(.appOrange, for: .normal)
            doseButton.backgroundColor = .clear
            doseButton.isUserInteractionEnabled = false
        }
        
        medicineLabel.textColor = color
        quantityLabel.textColor = color
        doseNumberLabel.textColor = color
    }
    
    func nextDoseButton()
    {
        guard let dose = self.dose else { return }
        
        guard let expireStr = dose.Medicine_finish_days else { return }
        
        let currentDate = Date()
        let expireDate = self.dateFromString(dateStr: expireStr)
        
        if currentDate >= expireDate
        {
            doseButton.setTitle("Expired".localized(), for: .normal)
            doseButton.setTitleColor(.appGreen, for: .normal)
            doseButton.backgroundColor = .clear
            doseButton.isUserInteractionEnabled = false
        }
        else
        {
//            let idPrefix = "Medicine_id_" + (dose.Medicine_id ?? "identifier")
//            
//            // Get Notification outside main thread then reload
//            NotifiyVM.shared.getNextNotificationDate(idPrefix: idPrefix) { [weak self] (nextTime) in
//                self?.nextDoseDate = nextTime
//                
//            }
            
            if let nextTime = self.nextDoseDate
            {
                print("nextTime: \(nextTime)")
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale.init(identifier: "en")
                dateFormatter.dateFormat = "dd MMM, h:mm a"
                let strDate = dateFormatter.string(from: nextTime)
                
//                doseButton.setTitle("NextDose".localized() + strDate, for: .normal)
                doseButton.setTitle(strDate, for: .normal)
                doseButton.setTitleColor(.appOrange, for: .normal)
                doseButton.backgroundColor = .clear
                doseButton.isUserInteractionEnabled = false
            }
            else
            {
                doseButton.setTitle("FirstDose".localized(), for: .normal)
                doseButton.setTitleColor(.white, for: .normal)
                doseButton.backgroundColor = .appGreen
                doseButton.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func firstDoseAction(_ sender: UIButton)
    {
//        self.firstDoseFunc()
        goToCalenderCV()
    }
    
    func goToCalenderCV()
    {
        guard let dose = dose else { return }
        
        vc?.goToCalenderCV(dose: dose)
    }
    
    func firstDoseFunc()
    {
//        let minutes = UserDefaults.standard.integer(forKey: "NotificationPeriod")
//        
//        guard let dose = self.dose else { return }
//        
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
//            
//            if granted == true
//            {
//                self.scheduleNotification(dose: dose, minuts: minutes)
//            }
//        }
//        vc?.getData()
    }
    
    func reminderAlert()
    {
        let ac = UIAlertController(title: "AskNotification".localized(), message: nil, preferredStyle: .alert)
        ac.addTextField { (textField) in
            textField.keyboardType = .numberPad
        }
        
        let okAction = UIAlertAction(title: "OK".localized(), style: .default) { [unowned ac] _ in
            if let askString = ac.textFields![0].text
            {
                if askString.count < 1 { return }
                guard let askInt = Int(askString) else { return }
                
                UserDefaults.standard.set(askInt, forKey: "NotificationPeriod")
                UserDefaults.standard.synchronize()
                
                self.firstDoseFunc()
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in }
        
        ac.addAction(okAction)
        ac.addAction(cancelAction)
        
        vc?.present(ac, animated: true, completion: nil)
    }
}

// MARK: - Notifications

extension DoseCell
{
    func dateFromString(dateStr: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: dateStr) ?? Date()
        return date
    }
}

class DateCell: JTAppleCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var selectedView: UIView!
}

class DateHeader: JTAppleCollectionReusableView  {
    @IBOutlet var monthTitle: UILabel!
}











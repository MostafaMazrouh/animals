//
//  DosesVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/8/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit
import UserNotifications


class DosesVC: UIViewController
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topLabel: UILabel!
    
    let vm = PrescriptionsVM()
    var doseArray = [Dose]()
    var prescId: String?
    var prescription: PrescriptionByOwner?
    
    var doseDate: String?
    var doseTime: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUp()
        
        guard let prescription = prescription else { return }
        
        let name = (MOLHLanguage.currentAppleLanguage() == "en") ? (prescription.ANIMAL_NAME_EN ?? "") : (prescription.ANIMAL_NAME_AR ?? "")
        
        self.topLabel.text = "\(name): \(prescription.PAT_INFO_ID ?? "")"
        
        print("prescription id: \(prescription.Presc_id ?? "-1")")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.getData()
    }
    
    func setUp()
    {
        self.title = NSLocalizedString("Doses", comment: "")
        self.addBackgroundImage()
    }
    
    func getData()
    {
        doseArray = [Dose]()
//        tableView.reloadData()
        
        guard let prescId = prescId else { return }
        
        vm.getDose(strPrescId: prescId) { [weak self] (doses, error) in
            
            guard let doseData = doses else { return Messager.show(message: "NoData".localized()) }
            
            self?.doseArray = doseData
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.layoutIfNeeded()
            }
        }
    }
    
    func goToCalenderCV(dose: Dose)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let calenderVC = storyboard.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        calenderVC.dose = dose
        calenderVC.prescId = prescId
        navigationController?.pushViewController(calenderVC, animated: true)
    }
    
    // localizedUserNotificationString(forKey:arguments:)
    func notify(dose: Dose)
    {
        let center = UNUserNotificationCenter.current()
//        center.getPendingNotificationRequests { (requestArray) in
//            for request in requestArray
//            {
//                if let calendarNotificationTrigger = request.trigger as? UNCalendarNotificationTrigger,
//                    let nextTriggerDate = calendarNotificationTrigger.nextTriggerDate()
//                {
//                    print(nextTriggerDate)
//                }
//            }
//        }
        center.getNotificationSettings { (settings) in
            guard settings.authorizationStatus != .authorized else {
                Messager.show(message: "Please allow local notifications for this app")
                return
            }
            
            let content = UNMutableNotificationContent()
            content.title = "Time for \(dose.Medicine_title ?? "Medicine")"
            content.body = "Dose amount: \(dose.Medicine_desc ?? "")"
            content.sound = UNNotificationSound.default()
            
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: true)
            
            let identifier = dose.Medicine_id ?? "identifier"
            let request = UNNotificationRequest(identifier: identifier,
                                                content: content,
                                                trigger: trigger)
            center.add(request, withCompletionHandler: { (error) in
                if let error = error {
                    Messager.show(message: "Couldn't add notification with error: \(error.localizedDescription)")
                }
            })
            trigger.nextTriggerDate()
        }
    }
}

// MARK: - UITableViewDelegate

extension DosesVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("number: \(self.doseArray.count)")
        return self.doseArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoseCell", for: indexPath) as! DoseCell
        
        cell.vc = self
        if indexPath.row == 0
        {
            cell.setUp()
        } else if (indexPath.row - 1) < self.doseArray.count {
            let dose = self.doseArray[(indexPath.row - 1)]
            
            let idPrefix = "Medicine_id_" + (dose.Medicine_id ?? "identifier")
            
            let dates = UserDefaults.standard.object(forKey: idPrefix) as? [Date]
            
            var nextTime: Date?
            for date in dates ?? [] {
                if date > Date() {
                    nextTime = date
                    break
                }
            }
            
            cell.nextDoseDate = nextTime
            cell.setUp(dose: dose)
        }
        
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        cell.layoutIfNeeded()
        
        return cell
    }
}




























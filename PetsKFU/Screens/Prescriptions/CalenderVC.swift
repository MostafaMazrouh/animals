//
//  CalenderVC.swift
//  PetsKFU
//
//  Created by Mostafa on 6/12/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation
import CVCalendar


class CalenderVC: UIViewController
{
    @IBOutlet weak var dateTimeContainer: UIView!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var selectedDateLabel: UILabel!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    var selectedDateString: String?
    var selectedTimeString: String?
    
    var selectedDate: Date?
    var selectedTime: Date?
    
    private var shouldShowDaysOut = true
    private var animationFinished = true
    private var selectedDay: DayView!
    private var currentCalendar: Calendar?
    
    weak var dosesVC: DosesVC?
    var dose: Dose?
    var doseVM = DosesVM()
    var prescId: String?
    
    override func awakeFromNib() {
//        let timeZoneBias = 480 // (UTC+08:00)
        
        currentCalendar = Calendar(identifier: .gregorian)
        currentCalendar?.locale = Locale(identifier: MOLHLanguage.currentAppleLanguage())
//        if let timeZone = TimeZone(secondsFromGMT: -timeZoneBias * 60) {
//            currentCalendar?.timeZone = timeZone
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
    }
    
    func setUp()
    {
        monthView.backgroundColor = .appOrange
        monthLabel.textColor = .white
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
        
        title = "\("Date".localized()) \("FirstDose".localized())"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: MOLHLanguage.currentAppleLanguage())
        formatter.dateFormat = "MMMM, yyyy"
        monthLabel.text = formatter.string(from: Date())
        
//        calendarView.toggleViewWithDate(Date())
        
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: MOLHLanguage.currentAppleLanguage())
        timePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        doneButton.setTitle("OK".localized(), for: .normal)
        
        bottomView.backgroundColor = UIColor.appGreen
        
        updateSelectedDateLabel()
    }
    
    @IBAction func doneAction(_ sender: UIButton)
    {
        guard let selectedTime = selectedTime,
            let selectedDate = selectedDate
            else { return }
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en")
        formatter.dateFormat = "HH:mm"
        let formatedTime = formatter.string(from: selectedTime)
        
        formatter.dateFormat = "dd/MM/yyyy"
        let formatedDate = formatter.string(from: selectedDate)
        
        print("formatedTime: \(formatedTime)")
        print("formatedDate: \(formatedDate)")
        
        dosesVC?.doseTime = formatedTime
        dosesVC?.doseDate = formatedDate
        
        guard let prescId = prescId, let dose = dose,
            let medicineId = dose.Medicine_id,
        let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId")
        else { return }
        
        view.isUserInteractionEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        doseVM.saveDoseToServer(strPrescID: prescId, strMedItemID: medicineId, doseDate: formatedDate, doseTime: formatedTime, strOwnerID: strOwnerID) { [weak self] (success, error) in
            
            DispatchQueue.main.async {
                print("CalenderVC:: doneAction:: success: \(String(describing: success))")
                
                self?.view.isUserInteractionEnabled = true
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                guard error == nil,
                    let success = success, success == true
                    else
                {return Messager.show(message: "Wrong".localized())}
                
                NotifiyVM.shared.scheduleDose(dose: dose)
                
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
}


// MARK: - CVCalendarViewDelegate & CVCalendarMenuViewDelegate

extension CalenderVC: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    // MARK: Required methods
    
    func presentationMode() -> CalendarMode { return .monthView }
    
    func firstWeekday() -> Weekday { return .saturday }
    
    // MARK: Optional methods
    
    func calendar() -> Calendar? { return currentCalendar }
    
    func dayOfWeekTextColor(by weekday: Weekday) -> UIColor {
        return UIColor.white
    }
    
    func shouldShowWeekdaysOut() -> Bool { return shouldShowDaysOut }
    
    // Defaults to true
    func shouldAnimateResizing() -> Bool { return true }
    
    func shouldAutoSelectDayOnMonthChange() -> Bool { return false }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        selectedDay = dayView
        selectedDateString = "\(selectedDay.date.commonDescription)"
        selectedDate = selectedDay.date.convertedDate()
        updateSelectedDateLabel()
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        if monthLabel.text != date.globalDescription && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            
            UIView.animate(withDuration: 0.35, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransform.identity
                
            }) { _ in
                
                self.animationFinished = true
                self.monthLabel.frame = updatedMonthLabel.frame
                self.monthLabel.text = updatedMonthLabel.text
                self.monthLabel.transform = CGAffineTransform.identity
                self.monthLabel.alpha = 1
                updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool { return false }
    
    func weekdaySymbolType() -> WeekdaySymbolType { return .short }
    
    //    func selectionViewPath() -> ((CGRect) -> (UIBezierPath)) {
    //        return { UIBezierPath(rect: CGRect(x: 0, y: 0, width: $0.width, height: $0.height)) }
    //    }
    
    func shouldShowCustomSingleSelection() -> Bool { return false }
    
    func preliminaryView(viewOnDayView dayView: DayView) -> UIView {
        let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.frame, shape: CVShape.circle)
        circleView.fillColor = .colorFromCode(0xCCCCCC)
        return circleView
    }
    
    func preliminaryView(shouldDisplayOnDayView dayView: DayView) -> Bool {
        if (dayView.isCurrentDay) {
            return true
        }
        return false
    }
    
    func supplementaryView(viewOnDayView dayView: DayView) -> UIView {
        
        dayView.setNeedsLayout()
        dayView.layoutIfNeeded()
        
        let π = Double.pi
        
        let ringLayer = CAShapeLayer()
        let ringLineWidth: CGFloat = 4.0
        let ringLineColour = UIColor.blue
        
        let newView = UIView(frame: dayView.frame)
        
        let diameter = (min(newView.bounds.width, newView.bounds.height))
        let radius = diameter / 2.0 - ringLineWidth
        
        newView.layer.addSublayer(ringLayer)
        
        ringLayer.fillColor = nil
        ringLayer.lineWidth = ringLineWidth
        ringLayer.strokeColor = ringLineColour.cgColor
        
        let centrePoint = CGPoint(x: newView.bounds.width/2.0, y: newView.bounds.height/2.0)
        let startAngle = CGFloat(-π/2.0)
        let endAngle = CGFloat(π * 2.0) + startAngle
        let ringPath = UIBezierPath(arcCenter: centrePoint,
                                    radius: radius,
                                    startAngle: startAngle,
                                    endAngle: endAngle,
                                    clockwise: true)
        
        ringLayer.path = ringPath.cgPath
        ringLayer.frame = newView.layer.bounds
        
        return newView
    }
    
    func dayOfWeekTextColor() -> UIColor { return .white }
    
    func dayOfWeekBackGroundColor() -> UIColor { return .appOrange }
    
    func disableScrollingBeforeDate() -> Date { return Date() }
    
    func maxSelectableRange() -> Int { return 1 }
    
    func earliestSelectableDate() -> Date {
        var dayComponents = DateComponents()
        dayComponents.day = -1
        let calendar = Calendar(identifier: .gregorian)
        if let earliestDate = calendar.date(byAdding: dayComponents, to: Date()) {
            return earliestDate
        }
        
        return Date()
    }
    
    func latestSelectableDate() -> Date {
        
        if let dose = dose,
            let finishDays = dose.Medicine_finish_days {
            
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en")
            formatter.dateFormat = "dd/MM/yyyy"
            let finishDaysDate = formatter.date(from: finishDays)
            
            return finishDaysDate ?? Date()
        }
        
        var dayComponents = DateComponents()
        dayComponents.day = 70
        let calendar = Calendar(identifier: .gregorian)
        if let lastDate = calendar.date(byAdding: dayComponents, to: Date()) {
            return lastDate
        }
        
        return Date()
    }
}


// MARK: - CVCalendarViewAppearanceDelegate

extension CalenderVC: CVCalendarViewAppearanceDelegate {
    
    func dayLabelWeekdayDisabledColor() -> UIColor { return .lightGray }
    
    func dayLabelPresentWeekdayInitallyBold() -> Bool { return false }
    
    func spaceBetweenDayViews() -> CGFloat { return 0 }
    
    func dayLabelFont(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIFont { return UIFont.systemFont(ofSize: 14) }
    
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (_, .selected, _), (_, .highlighted, _): return ColorsConfig.selectedText
        case (.friday, .in, _): return ColorsConfig.sundayText
        case (.friday, _, _): return ColorsConfig.sundayTextDisabled
        case (_, .in, _): return ColorsConfig.text
        default: return ColorsConfig.textDisabled
        }
    }
    
    func dayLabelBackgroundColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (.friday, .selected, _), (.friday, .highlighted, _): return ColorsConfig.sundaySelectionBackground
        case (_, .selected, _), (_, .highlighted, _): return ColorsConfig.selectionBackground
        default: return nil
        }
    }
}

struct ColorsConfig {
    static let selectedText = UIColor.white
    static let text = UIColor.black
    static let textDisabled = UIColor.gray
    static let selectionBackground = UIColor(red: 0.2, green: 0.2, blue: 1.0, alpha: 1.0)
    static let sundayText = UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1.0)
    static let sundayTextDisabled = UIColor(red: 1.0, green: 0.6, blue: 0.6, alpha: 1.0)
    static let sundaySelectionBackground = sundayText
}


// MARK: - Time Picker

extension CalenderVC
{
    @objc func dateChanged(_ sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: MOLHLanguage.currentAppleLanguage())
        formatter.dateFormat = "h:mm a"
        
        selectedTimeString = formatter.string(from: sender.date)
        selectedTime = sender.date
        
        updateSelectedDateLabel()
    }
    
    func updateSelectedDateLabel()
    {
        selectedDateLabel.text = "\(selectedDateString ?? "")" + "   \(selectedTimeString ?? "")"
        
        if selectedDateString != nil, selectedTimeString != nil {
            doneButton.setTitleColor(.white, for: .normal)
            doneButton.isUserInteractionEnabled = true
        } else {
            doneButton.setTitleColor(.gray, for: .normal)
            doneButton.isUserInteractionEnabled = false
        }
    }
}


























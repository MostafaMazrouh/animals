//
//  DosesModels.swift
//  PetsKFU
//
//  Created by Mostafa on 1/21/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

struct PrescriptionByOwner: Codable
{
    let Presc_id: String?
    let Presc_date_ar: String?
    let Presc_date_en: String?
    let Presc_clinic: String?
    let Presc_clinic_en: String?
    let Presc_doctor: String?
    let Presc_doctor_en: String?
    let ANIMAL_NAME_AR: String?
    let ANIMAL_NAME_EN: String?
    let PAT_INFO_DOB_EN: String?
    let PAT_INFO_ID: String?
}

struct Dose: Codable
{
    let Medicine_id: String?
    let Medicine_title: String?
    let Medicine_desc: String?
    let Medicine_dose_ar: String? // Use
    let Medicine_dose_en: String? // Don't use
    let Medicine_dose_code: String?
    let Medicine_finish_days: String?   // "29/06/2019",
    let first_dose_date: String?        //"03/06/2019 15:41:00"
}






















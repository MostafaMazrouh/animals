//
//  PrescriptionsVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/21/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class PrescriptionsVM
{
    private let crModel = CodableRESTModel()
    
    func getPrescriptions(ownerId: String, handler: @escaping ([PrescriptionByOwner]?, Error?)->())
    {
        let url = BASE_URL + "/GetPrescriptionsByOwner"
        let httpBody = ["strOwnerID": ownerId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let qaData = self?.crModel.decode(toType: [PrescriptionByOwner].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(qaData, nil)
        }
    }
    
    func getDose(strPrescId: String, handler: @escaping ([Dose]?, Error?)->())
    {
        let url = BASE_URL + "/GetMedicines"
        let httpBody = ["strPrescId": strPrescId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let doseData = self?.crModel.decode(toType: [Dose].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(doseData, nil)
        }
    }
}






















//
//  PrescriptionsVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/8/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit
import UserNotifications

class PrescriptionsVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    let vm = PrescriptionsVM()
    var prescription = [PrescriptionByOwner]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Prescriptions", comment: "")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.addBackgroundImage()
        
        guard let userId = UserDefaults.standard.string(forKey:"OwnerId") else
        { return }
        
        vm.getPrescriptions(ownerId: userId) { [weak self] (data, error) in
            guard let prescriptionData = data else
                {return Messager.show(message: "Wrong".localized())}
//            { return Messager.show(message: error?.localizedDescription) }
            
            self?.prescription = prescriptionData
            
            DispatchQueue.main.async { self?.tableView.reloadData() }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("number: \(self.prescription.count)")
        return self.prescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrescriptionByOwnerCell", for: indexPath) as! PrescriptionByOwnerCell
        
        if indexPath.row < self.prescription.count {
            cell.prescription = self.prescription[indexPath.row]
            cell.setUp()
        }
        
        cell.owner = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
}

class PrescriptionByOwnerCell: UITableViewCell
{
    @IBOutlet weak var animalNameLabel: UILabel!
    @IBOutlet weak var clinicLabel: UILabel!
    @IBOutlet weak var animalImage: UIImageView!
    @IBOutlet weak var animalView: UIView!
    
    var prescription: PrescriptionByOwner?
    weak var owner: PrescriptionsVC?
    
    
    func setUp()
    {
        animalView.setRounded()
        
        print("prescription: \(String(describing: prescription))")
        
        if MOLHLanguage.currentAppleLanguage() == "en"
        {
            animalNameLabel.text = "\(prescription?.ANIMAL_NAME_EN ?? ""): \(prescription?.PAT_INFO_ID ?? "")"
            clinicLabel.text = "\(prescription?.Presc_clinic_en ?? "") , \(prescription?.Presc_doctor_en ?? "")"
        } else {
            animalNameLabel.text = "\(prescription?.ANIMAL_NAME_AR ?? ""): \(prescription?.PAT_INFO_ID ?? "")"
            clinicLabel.text = "\(prescription?.Presc_clinic ?? "") , \(prescription?.Presc_doctor ?? "")"
        }
        
        animalNameLabel.textColor = .appGreen
        clinicLabel.textColor = .appGreen
    }
    
    @IBAction func selectAction(_ sender: FitButton)
    {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let dosesVC = mainSB.instantiateViewController(withIdentifier: "DosesVC") as! DosesVC
        print("Selected Presc_id: \(prescription?.Presc_id ?? "-1")")
        dosesVC.prescId = prescription?.Presc_id
        dosesVC.prescription = prescription
        owner?.navigationController?.pushViewController(dosesVC, animated: true)
        
//        self.getAddedNotifications()
    }
    
    func getAddedNotifications()
    {
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests { (requestArray) in
            print("Pending Count: \(requestArray)")
            for request in requestArray
            {
                print("request: \(request.identifier)")
                //                if let calendarNotificationTrigger = request.trigger as? UNCalendarNotificationTrigger,
                //                    let nextTriggerDate = calendarNotificationTrigger.nextTriggerDate()
                //                {
                //                    print(nextTriggerDate)
                //                }
            }
        }
    }
}














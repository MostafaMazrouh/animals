//
//  AppointmentsVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/19/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class AppointmentsVM
{
    private let crModel = CodableRESTModel()
    
    var appointmentsArray = [Appointment]()
    var reservedsArray = [Reserved]()
    weak var vc: AppointmentsVC?
    
    func getAppointments(strDoctorId: String, strDate: String, handler: @escaping ([Appointment]?, Error?)->())
    {
        let url = BASE_URL + "/GetAppointments"
        let httpBody = ["strDoctorId": strDoctorId,
                        "strDate": strDate]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let reserved = self?.crModel.decode(toType: [Appointment].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(reserved, nil)
        }
    }
    
    func reserveAppointment(strOwnerID: String, strReserveId: String, handler: @escaping (String?)->())
    {
        let url = BASE_URL + "/ReserveAppointment"
        let httpBody = ["strOwnerID": strOwnerID,
                        "strReserveId": strReserveId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil) }
            guard let mdata = data else { return handler(nil) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil) }
            
            guard let cleanString = String(data: cleanData, encoding: String.Encoding.utf8) as String? else { return }
            
            print("reserveAppointment result: \(cleanString)")
            handler(cleanString)
        }
    }
    
    func getReservedAppointments(strPatientId: String, handler: @escaping ([Reserved]?, Error?)->())
    {
        let url = BASE_URL + "/GetReservedAppointments"
        let httpBody = ["strOwnerID": strPatientId]
        
        print("url: \(url)")
        print("httpBody: \(httpBody)")
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let reserved = self?.crModel.decode(toType: [Reserved].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(reserved, nil)
        }
    }
    
    func cancelAppointment(strReserveId: String, handler: @escaping (String?)->())
    {
        let url = BASE_URL + "/CancelAppointment"
        let httpBody = ["strReserveId": strReserveId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil) }
            guard let mdata = data else { return handler(nil) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil) }
            
            guard let cleanString = String(data: cleanData, encoding: String.Encoding.utf8) as String? else { return }
            
            print("CancelAppointment result: \(cleanString)")
            handler(cleanString)
        }
    }
}

// MARK: - MainTableView

extension AppointmentsVM
{
    func numberOfSections() -> Int { return 4 }
    
    func numberOfRowsInSection (section: Int) -> Int
    {
        switch section {
        case 0: return 2
        case 1: return 1
        case 2: return (appointmentsArray.count > 0) ? (appointmentsArray.count + 1) : 0
        case 3: return (reservedsArray.count > 0) ? (reservedsArray.count + 1) : 0
        default: return 0
        }
    }
    
    func heightForRowAt (indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section {
        case 0: return 80
        case 1: return 300
        case 2: return 40
        case 3: return 80
            
        default: return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAt indexPath: IndexPath, vc: AppointmentsVC?) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCell", for: indexPath) as! ChooseCell
            
            if indexPath.row == 0 {
                cell.setUp(type: .Clinic, vc: vc, clinic: vc?.selectedClinic)
            } else {
                cell.setUp(type: .Doctor, vc: vc, doctor: vc?.selectedDoctor)
            }
            return cell
        }
        
        
        if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
            
            let newCalendar = BSIslamicCalendar()
            newCalendar?.delegate = vc
            
            if MOLHLanguage.currentAppleLanguage() == "en" {
                newCalendar?.setIslamicDatesInArabicLocale(false)
            } else {
                newCalendar?.setIslamicDatesInArabicLocale(true)
            }
            
            newCalendar?.setShowIslamicMonth(true)
//            newCalendar?.setShortInitials(true)
            
            
            
            newCalendar?.setDateBGColor(.clear)
            newCalendar?.setDateTextColor(.white)
        newCalendar?.setSelectedDateBGColor(UIColor.appOrange.withAlphaComponent(1))
            newCalendar?.setDaysNameColor(.white)
            newCalendar?.setCurrentDateTextColor(.black)
            cell.contentView.backgroundColor = UIColor.appGreen.withAlphaComponent(0.3)
            
            let widthConstraint = newCalendar?.widthAnchor.constraint(equalToConstant: 300)
            let heightConstraint = newCalendar?.heightAnchor.constraint(equalToConstant: 300)
            newCalendar?.addConstraints([widthConstraint!, heightConstraint!])
            
            cell.stackView.addArrangedSubview(newCalendar!)
            
            return cell
        }
        
        let alpha: CGFloat = ((indexPath.row%2)==1) ? 0.25 : 0.5
        
        if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentCell", for: indexPath) as! AppointmentCell
            
            if indexPath.row == 0 {
                cell.contentView.backgroundColor = .appGreen
                cell.setUp(appointment: nil, vc: vc)
            } else if (indexPath.row-1) < appointmentsArray.count {
                cell.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(alpha)
                cell.setUp(appointment: appointmentsArray[indexPath.row-1], vc: vc)
                
            }
            return cell
        }
        
        if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReservedCell", for: indexPath) as! ReservedCell
            
            cell.vc = self.vc
            if indexPath.row == 0 {
                cell.contentView.backgroundColor = .appGreen
                cell.setUp()
            } else {
                cell.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(alpha)
                if (indexPath.row-1) < reservedsArray.count {
                    cell.setUp(reserved: reservedsArray[indexPath.row-1])
                }
            }
            return cell
        }
        
        return UITableViewCell()
    }
}
























//
//  AppointmentsViws.swift
//  PetsKFU
//
//  Created by Mostafa on 1/19/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation


class ChooseCell: UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameButton: UIButton!
    weak var vc: AppointmentsVC?
    var type = SelectType.Clinic
    
    func setUp(type: SelectType, vc: AppointmentsVC?, clinic: Clinic? = nil, doctor: Doctor? = nil)
    {
        self.type = type
        self.vc = vc
        
        nameLabel.text = (type == SelectType.Clinic) ?
            "Clinic".localized() : "Doctor".localized()
        
        if let clinic = clinic
        {
            let text = (MOLHLanguage.currentAppleLanguage() == "en") ?
                (clinic.Clinic_title_en ?? "") :
                (clinic.Clinic_title_ar ?? "")
            
            nameButton.setTitle(text, for: .normal)
            return
        }
        
        if let doctor = doctor
        {
            let text = (MOLHLanguage.currentAppleLanguage() == "en") ?
            (doctor.doctor_name_en ?? "") :
            (doctor.doctor_name_ar ?? "")
            nameButton.setTitle(text, for: .normal)
            return
        }
    }
    
    @IBAction func chooseAction(_ sender: UIButton)
    {
        vc?.selectVM.selectType = type
        vc?.showSelectTV(show: true)
        
        if type == SelectType.Clinic {
            
        } else if type == SelectType.Doctor {
            
        }
    }
}

class AppointmentCell: UITableViewCell
{
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var reserveButton: UIButton!
    
    var appointment: Appointment?
    let vm = AppointmentsVM()
    weak var vc: AppointmentsVC?
    
    func setUp(appointment: Appointment?, vc: AppointmentsVC?)
    {
        guard let appointment = appointment else {
            startLabel.text = "Startdate".localized()
            reserveButton.isHidden = true
            return
        }
        self.appointment = appointment
        self.vc = vc
        
        startLabel.text = appointment.Reservation_Start
        
        reserveButton.backgroundColor = .appOrange
        reserveButton.setTitle("Reserve".localized(), for: .normal)
        reserveButton.isHidden = false
    }
    
    @IBAction func reserveAction(_ sender: UIButton)
    {
        let alertController = UIAlertController(title: "SureReserve".localized(), message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertActionStyle.default) { UIAlertAction in
            
            self.reserve()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (alert) in }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.vc?.present(alertController, animated: true, completion: nil)
        
//        self.reserve()
    }
    
    func reserve()
    {
        guard let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId") else
        { return }
        
        guard let strReserveId = appointment?.Reservation_id else { return }
        
        vm.reserveAppointment(strOwnerID: strOwnerID, strReserveId: strReserveId) { [weak self] (result) in
            
            DispatchQueue.main.async {
                guard let result = result else { return Messager.show(message: "ReservationError".localized()) }
                
                if result == "0" {
                    Messager.show(message: "ReservationCompleted".localized())
                    self?.vc?.getReservedAppointments()
                    self?.vc?.getAppointments()
                }
                else if result == "-1000"
                {
                    Messager.show(message: "ReservationSame".localized())
                }
                else {
                    // if result > "8"
                    Messager.show(message: "ReservationLimit".localized())
                }
                
            }
        }
    }
}

class ReservedCell: UITableViewCell
{
    @IBOutlet weak var clinicLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    let vm = AppointmentsVM()
    var reserved: Reserved?
    weak var vc: AppointmentsVC?
    
    func setUp(reserved: Reserved? = nil)
    {
        self.reserved = reserved
        guard let reserved = reserved else {
            clinicLabel.text = "Clinic".localized()
            doctorLabel.text = "Doctor".localized()
            dateLabel.text = "Startdate".localized()
            cancelButton.setTitle("", for: .normal)
            return
        }
        
        clinicLabel.text = reserved.Reservation_Dept
        doctorLabel.text = reserved.Reservation_Doctor
        dateLabel.text = reserved.Reservation_Date
        cancelButton.setTitle("Cancell".localized(), for: .normal)
    }
    
    @IBAction func cancelAction(_ sender: UIButton)
    {
        guard let reserved = reserved else { return }
        guard let reservationId = reserved.Reservation_id else { return }
        
        
        let alertController = UIAlertController(title: "Sure".localized(), message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertActionStyle.default) { UIAlertAction in
            
            self.vm.cancelAppointment(strReserveId: reservationId) { [weak self] (result) in
                print("cancelAction result: \(result ?? "No")")
                if let result = result, result == "1"
                {
                    Messager.show(message: "ReservationCancelled".localized())
                    DispatchQueue.main.async {
                        self?.vc?.getReservedAppointments()
                        self?.vc?.getAppointments()
                    }
                } else {
                    Messager.show(message: "CancelationError".localized())
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (alert) in }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.vc?.present(alertController, animated: true, completion: nil)
    }
}

class CalendarCell: UITableViewCell
{
    @IBOutlet weak var stackView: UIStackView!
}

























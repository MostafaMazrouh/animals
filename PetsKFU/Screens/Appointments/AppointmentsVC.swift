//
//  AppointmentsVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/9/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class AppointmentsVC: UIViewController
{
    @IBOutlet weak var mainTV: UITableView!
    @IBOutlet weak var selectTV: UITableView!
    @IBOutlet weak var selectTVView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var selectTVViewIsShown = false
    
    let vm = AppointmentsVM()
    let selectVM = SelectVM()
    
    var strSelectedDate = ""
    
    var selectedClinic: Clinic? {
        didSet {
            guard let strClinicId = selectedClinic?.Clinic_id else { return }
            mainTV.reloadSections([0], with: .none)
            selectVM.getDoctors(strClinicId: strClinicId) { [weak self] (doctors, error) in
                guard let doctors = doctors else
                    {return Messager.show(message: "Wrong".localized())}
                
                DispatchQueue.main.async {
                    self?.selectVM.doctorsArray = doctors
                    self?.selectedDoctor = (doctors.count > 0) ? doctors[0] : nil
                    self?.mainTV.reloadSections([0], with: .none)
                }
            }
        }
    }
    
    var selectedDoctor: Doctor?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUp()
        self.getReservedAppointments()
        self.getClinics()
    }
    
    private func setUp()
    {
        addBackgroundImage()
//        hideKeyboardWhenTappedAround()
        adjustBackButton()
        
        vm.vc = self
        showSelectTV(show: false)
        self.title = "Appointments".localized()
        mainTV.tag = TableTag.Main.rawValue
        selectTV.tag = TableTag.Select.rawValue
        
        searchBar.showsCancelButton = true
        searchBar.delegate = self
    }
    
    func adjustBackButton()
    {
        self.navigationItem.hidesBackButton = true
        
        let imageName = Constants.isAR ? "BackAr" : "Back"
        let newBackButton = UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action: #selector(self.back(_:)))
        newBackButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(_ sender: UIBarButtonItem)
    {
        dismissKeyboard()
        if selectTVViewIsShown {
            showSelectTV(show: false)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getReservedAppointments()
    {
        guard let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId") else
        {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
            return
        }
        
        vm.getReservedAppointments(strPatientId: strOwnerID) { [weak self] (reserved, error) in
            
            guard let reserved = reserved else
                {return Messager.show(message: "NoReserved".localized())}
            
            DispatchQueue.main.async {
                self?.vm.reservedsArray = reserved
                self?.mainTV.reloadSections([2,3], with: .none)
            }
        }
    }
    
    private func getClinics()
    {
        selectVM.getClinics { [weak self] (clinics, error) in
            guard let clinics = clinics else
                {return Messager.show(message: "Wrong".localized())}
            
            DispatchQueue.main.async {
                self?.selectVM.clinicsArray = clinics
                self?.selectedClinic = (clinics.count > 0) ? clinics[0] : nil
                self?.mainTV.reloadSections([2,3], with: .none)
            }
        }
    }
    
    func showSelectTV(show: Bool)
    {
        selectTVViewIsShown = show
        
        if !show {
            mainTV.reloadData()
            view.sendSubview(toBack: selectTVView)
        } else {
            selectTV.reloadData()
            view.bringSubview(toFront: selectTVView)
        }
    }
}


extension AppointmentsVC: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView.tag == TableTag.Main.rawValue {
            return vm.numberOfSections()
        } else {
            return selectVM.numberOfSections()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == TableTag.Main.rawValue {
            return vm.numberOfRowsInSection(section: section)
        } else {
            return selectVM.numberOfRowsInSection(section: section)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView.tag == TableTag.Main.rawValue {
            return vm.heightForRowAt(indexPath: indexPath)
        } else {
            return selectVM.heightForRowAt(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == TableTag.Main.rawValue {
            return vm.tableView(tableView:tableView, cellForRowAt: indexPath, vc: self)
        } else {
            return selectVM.tableView(tableView:tableView, cellForRowAt: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView.tag == TableTag.Main.rawValue {
//            vm.numberOfSections()
        } else {
            selectVM.tableView(tableView: tableView, didSelectRowAt: indexPath, vc: self)
            searchBar.text = ""
            dismissKeyboard()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat { return 20 }
}


extension AppointmentsVC: BSIslamicCalendarDelegate
{
    func islamicCalendar(_ calendar: BSIslamicCalendar!, shouldSelect date: Date!) -> Bool
    {
        return true
    }
    
    func islamicCalendar(_ calendar: BSIslamicCalendar!, dateSelected date: Date!, withSelectionArray selectionArry: [Any]!)
    {
        let dateNow = Date()
        
        if date.compare(dateNow) == .orderedAscending {
            
            Messager.show(message: "DayPassed".localized())
            return
        }
        
        let dateFormatter = DateFormatter()
        let islamicCalendar = Calendar.init(identifier: Calendar.Identifier.islamicUmmAlQura)
        dateFormatter.calendar = islamicCalendar
        dateFormatter.locale = Locale.init(identifier: "en") // This is used to show numbers, day and month in arabic letters
        dateFormatter.dateFormat = "dd/MM/yyyy"
        strSelectedDate = dateFormatter.string(from: date)
        
        print("strSelectedDate :: \(strSelectedDate)")
        
        getAppointments()
    }
    
    
    
    func getAppointments()
    {
        guard let strDoctorId = selectedDoctor?.doctor_id else { return }
        
        vm.getAppointments(strDoctorId: strDoctorId, strDate: strSelectedDate) { [weak self] (appointments, error) in
            
            guard let appointments = appointments else {
                return DispatchQueue.main.async {
                    self?.vm.appointmentsArray = [Appointment]()
                    self?.mainTV.reloadSections([2,3], with: .none)
                }
            }
            
            DispatchQueue.main.async {
                self?.vm.appointmentsArray = appointments
                self?.mainTV.reloadSections([2,3], with: .none)
            }
        }
        
    }
}

extension AppointmentsVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchTextStr: String)
    {
        let searchText = searchTextStr.lowercased()
        
        if selectVM.selectType == SelectType.Clinic {
            
            if searchText.count < 1 {
                selectVM.clinicsArraySearch = selectVM.clinicsArray
            }
            else {
            selectVM.clinicsArraySearch = selectVM.clinicsArray.filter { (clinic) -> Bool in
                
                ((clinic.Clinic_title_ar ?? "").lowercased().contains(searchText) || (clinic.Clinic_title_en ?? "").lowercased().contains(searchText))
                
            }
            }
        }
        else {
            if searchText.count < 1 {
                selectVM.doctorsArraySearch = selectVM.doctorsArray
            }
            else {
                selectVM.doctorsArraySearch = selectVM.doctorsArray.filter { (doctor) -> Bool in
                    
                    ((doctor.doctor_name_ar ?? "").lowercased().contains(searchText) || (doctor.doctor_name_en ?? "").lowercased().contains(searchText))
                    
                }
            }
        }
        
        selectTV.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
}













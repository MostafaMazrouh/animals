//
//  AppointmentsModels.swift
//  PetsKFU
//
//  Created by Mostafa on 1/19/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

struct Appointment: Codable
{
    let Reservation_id: String?
    let Reservation_Title: String?
    let Reservation_Start: String?
    let Reservation_End: String?
    let Reservation_Notes_ar: String?
    let Reservation_Notes_en: String?
    let Reservation_Date: String?
    let Reservation_Status: String?
    let Clinic_Title_Ar: String?
    let Clinic_Title_En: String?
    let Doctor_Name_Ar: String?
    let Doctor_Name_EN: String?
}

struct Reserved: Codable
{
    let Reservation_id: String?
    let Reservation_Dept: String?
    let Reservation_Doctor: String?
    let Reservation_Start: String?
    let Reservation_End: String?
    let Reservation_Date: String?
}

struct Clinic: Codable
{
    let Clinic_id: String?
    let Clinic_title_ar: String?
    let Clinic_title_en: String?
}

struct Doctor: Codable
{
    let doctor_id: String?
    let doctor_name_ar: String?
    let doctor_name_en: String?
}

enum TableTag: Int {
    case Main = 19
    case Select = 20
}

enum SelectType
{
    case Clinic
    case Doctor
}


















//
//  SelectVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/19/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class SelectVM
{
    private let crModel = CodableRESTModel()
    
    var clinicsArray = [Clinic]() {
        didSet {
            clinicsArraySearch = clinicsArray
        }
    }
    
    var doctorsArray = [Doctor]() {
        didSet {
            doctorsArraySearch = doctorsArray
        }
    }
    
    var clinicsArraySearch = [Clinic]()
    var doctorsArraySearch = [Doctor]()
    
    var selectType = SelectType.Clinic
    
    func getClinics(handler: @escaping ([Clinic]?, Error?)->())
    {
        let url = BASE_URL + "/GetClinics"
        
        crModel.getData(url: url, receivedType: [Clinic].self) { [weak self] (clinics, error) in
            
            self?.clinicsArray = clinics ?? []
            handler(clinics, error)
        }
    }
    
    func getDoctors(strClinicId: String, handler: @escaping ([Doctor]?, Error?)->())
    {
        let url = BASE_URL + "/GetDoctors"
        let httpBody = ["strClinicId": strClinicId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let doctors = self?.crModel.decode(toType: [Doctor].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(doctors, nil)
        }
    }
}

// MARK: - MainTableView

extension SelectVM
{
    func numberOfSections() -> Int { return 1 }
    
    func numberOfRowsInSection (section: Int) -> Int
    {
        if selectType == SelectType.Clinic { return clinicsArraySearch.count }
        else { return doctorsArraySearch.count }
    }
    
    func heightForRowAt (indexPath: IndexPath) -> CGFloat { return 60 }
    
    func tableView(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCell", for: indexPath)
        
        var text = ""
        if selectType == .Clinic {
            
            let clinic = clinicsArraySearch[indexPath.row]
            text = (clinic.Clinic_title_en ?? "") + " - " + (clinic.Clinic_title_ar ?? "")
            cell.textLabel?.text = text
            
        } else if selectType == .Doctor {
            
            let doctor = doctorsArraySearch[indexPath.row]
            text = (doctor.doctor_name_en ?? "") + " - " + (doctor.doctor_name_ar ?? "")
            
        }
        cell.textLabel?.text = text
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.lineBreakMode = .byWordWrapping
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAt indexPath: IndexPath, vc :AppointmentsVC)
    {
        if selectType == .Clinic {
            let clinic = clinicsArraySearch[indexPath.row]
            vc.selectedClinic = clinic
        } else if selectType == .Doctor {
            let doctor = doctorsArraySearch[indexPath.row]
            vc.selectedDoctor = doctor
        }
        vc.showSelectTV(show: false)
    }
}



















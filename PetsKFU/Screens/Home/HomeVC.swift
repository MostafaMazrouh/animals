//
//  HomeVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/1/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit
import UserNotifications

class HomeVC: UIViewController
{
    @IBOutlet weak var personalProfileLabel: UILabel!
    @IBOutlet weak var vaccinesLabel: UILabel!
    @IBOutlet weak var appointmentsLabel: UILabel!
    @IBOutlet weak var visitsLabel: UILabel!
    @IBOutlet weak var prescriptionsLabel: UILabel!
    @IBOutlet weak var firstaidsLabel: UILabel!
    
    @IBOutlet weak var personalProfileButton: UIButton!
    @IBOutlet weak var vaccinesButton: UIButton!
    @IBOutlet weak var appointmentsButton: UIButton!
    @IBOutlet weak var visitsButton: UIButton!
    @IBOutlet weak var prescriptionsButton: UIButton!
    @IBOutlet weak var firstaidsButton: UIButton!
    
    @IBOutlet var roundedViews: [UIView]!
    
    @IBOutlet weak var sideTableView: UITableView!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var leadingSideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var darkView: UIView!
    var isSideViewVisible = false
    
    let mainSB = UIStoryboard(name: "Main", bundle: nil)
    
    // MARK: - Side View Logic
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setup()
        
        self.setupSideView()
        
        self.login()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sideTableView.reloadData()
    }
    
    func setup()
    {
        self.title = NSLocalizedString("Home", comment: "")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.setLable(label: personalProfileLabel, key: "Personal Profile")
        self.setLable(label: vaccinesLabel, key: "Vaccine")
        self.setLable(label: appointmentsLabel, key: "Appointments")
        self.setLable(label: visitsLabel, key: "Visits")
        self.setLable(label: prescriptionsLabel, key: "Prescriptions")
        self.setLable(label: firstaidsLabel, key: "First aids")
        
        self.tintButton(button: personalProfileButton, imageName: "OwnerFile")
        self.tintButton(button: vaccinesButton, imageName: "Instructions")
        self.tintButton(button: appointmentsButton, imageName: "Schedule")
        self.tintButton(button: visitsButton, imageName: "Visits")
        self.tintButton(button: prescriptionsButton, imageName: "MedicalDescribe")
        self.tintButton(button: firstaidsButton, imageName: "FirstAids")
        
        for view in roundedViews { view.borderColor  = .appOrange }
    }
    
    func tintButton(button: UIButton, imageName: String, color: UIColor = UIColor.appGreen)
    {
        button.template(imageName: imageName, tintColor: color)
    }
    
    func setLable(label: UILabel, key: String)
    {
        label.text = key.localized()
        label.textColor = .black
    }
    
    func setupSideView()
    {
        // Side View
        let sideViewButton = UIButton()
        self.tintButton(button: sideViewButton, imageName: "menu", color: .white)
        sideViewButton.addTarget(self, action: #selector(self.sideViewAction(_:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: sideViewButton)
        
        self.changeSideViewVisiblity()
    }
    
    func login()
    {
        if (UserDefaults.standard.string(forKey:"OwnerId") == nil)
        {
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = mainSB.instantiateViewController(withIdentifier: "LoginVC")
            self.present(loginVC, animated: false, completion: nil)
        } else {
        }
    }
}


// MARK: - Side View Logic

extension HomeVC
{
    @IBAction func sideViewAction(_ sender: UIBarButtonItem)
    {
        self.changeSideViewVisiblity()
    }
    
    @IBAction func tapDarkAction(_ sender: UITapGestureRecognizer)
    {
        self.changeSideViewVisiblity()
    }
    
    func changeSideViewVisiblity()
    {
        
        if isSideViewVisible {
            leadingSideViewConstraint.constant = 0
            self.darkView.alpha = 0
        } else {
            leadingSideViewConstraint.constant = -280
            self.darkView.alpha = 1
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            
            if self.isSideViewVisible {
                self.darkView.alpha = 1
            } else {
                self.darkView.alpha = 0
            }
            
            self.view.layoutIfNeeded()
            self.isSideViewVisible = !self.isSideViewVisible
            self.view.endEditing(true)
        })
    }
}


// MARK: - Main Actions

extension HomeVC
{
    @IBAction func personalProfileAction(_ sender: UIBarButtonItem)
    {
        let personalProfileVC = mainSB.instantiateViewController(withIdentifier: "PersonalProfileVC") as! PersonalProfileVC
        self.navigationController?.pushViewController(personalProfileVC, animated: true)
    }
    
    @IBAction func vaccinesAction(_ sender: UIBarButtonItem)
    {
        let vaccinesVC = mainSB.instantiateViewController(withIdentifier: "VaccinesVC") as! VaccinesVC
        self.navigationController?.pushViewController(vaccinesVC, animated: true)
    }
    
    @IBAction func appointmentsVCAction(_ sender: UIBarButtonItem)
    {
        let appointmentsVC = mainSB.instantiateViewController(withIdentifier: "AppointmentsVC") as! AppointmentsVC
        self.navigationController?.pushViewController(appointmentsVC, animated: true)
    }
    
    @IBAction func VisitsVCAction(_ sender: UIBarButtonItem)
    {
        let visitsVC = mainSB.instantiateViewController(withIdentifier: "VisitsVC") as! VisitsVC
        self.navigationController?.pushViewController(visitsVC, animated: true)
    }
    
    @IBAction func prescriptionsVCAction(_ sender: UIBarButtonItem)
    {
        let prescriptionsVC = mainSB.instantiateViewController(withIdentifier: "PrescriptionsVC") as! PrescriptionsVC
        self.navigationController?.pushViewController(prescriptionsVC, animated: true)
    }
    
    @IBAction func firstAidsAction(_ sender: UIBarButtonItem)
    {
        let firstAidsVC = mainSB.instantiateViewController(withIdentifier: "FirstAidsVC") as! FirstAidsVC
        self.navigationController?.pushViewController(firstAidsVC, animated: true)
    }
}


// MARK: - UITableViewDelegate

extension HomeVC: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell: SetUpCell
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell", for: indexPath) as! LanguageCell
        } else if indexPath.row == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "LogoutCell", for: indexPath) as! LogoutCell
            cell.owner = self
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "AlertMeCell", for: indexPath) as! AlertMeCell
            cell.owner = self   
        }
        
        cell.setUp()
        
        return cell
    }
}







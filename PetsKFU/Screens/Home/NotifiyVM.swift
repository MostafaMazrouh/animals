//
//  NotifiyVM.swift
//  PetsKFU
//
//  Created by Mostafa on 6/10/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation
import UserNotifications



class NotifiyVM
{
    static let shared = NotifiyVM()
    
    let dosesVM = DosesVM()
    
    var notifiyMinutes: Int {
        get {
            return UserDefaults.standard.integer(forKey: "NotificationPeriod")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "NotificationPeriod")
            UserDefaults.standard.synchronize()
            dosesVM.getNotificationsOnServer()
        }
    }
    
    func scheduleDose(dose: Dose)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en")
        
        // Get start date
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let startDate = dateFormatter.date(from: dose.first_dose_date ?? "") ?? Date()
        
        // Get end date
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let endDate = dateFormatter.date(from: dose.Medicine_finish_days ?? "") ?? Date()
        
        var curDate = startDate
        
        // Get interval in hours
        guard let intervalHours = Int(dose.Medicine_dose_ar ?? "0"),
            intervalHours > 0
            else { return }
        let intervalMinutes = intervalHours * 60
        let intervalSecond = intervalMinutes * 60
        let interval: TimeInterval = TimeInterval(intervalSecond)
        
        var count: Int = 0
        let idPrefix = "Medicine_id_" + (dose.Medicine_id ?? "identifier")
        
        var dates = [Date]()
        
        while curDate <= endDate {
            let identifier = "\(idPrefix)_\(count)"
            scheduleNotification(with: identifier, datePure: curDate, dose: dose)
            dates.append(curDate)
            
            curDate = curDate.addingTimeInterval(interval)
            count += 1
        }
        
        // Store Array date in User Defaults
        UserDefaults.standard.set(dates, forKey: idPrefix)
        UserDefaults.standard.synchronize()
    }
    
    private func scheduleNotification(with identifier: String, datePure: Date,  dose: Dose)
    {
        let notifiySeconds = -TimeInterval(notifiyMinutes * 60)
        
        let date = datePure.addingTimeInterval(notifiySeconds)
        
        print("datePure: \(datePure)")
        print("date: \(date)")
        
        let content = UNMutableNotificationContent()
        content.title = "Time for \(dose.Medicine_title ?? "Medicine")"
        content.body = "Dose amount: \(dose.Medicine_desc ?? "")"
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "Dose"
        
        let triggerTime = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        print("triggerTime: \(triggerTime)")
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerTime, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }
    
    func getNextNotificationDate(idPrefix: String, date: @escaping (Date?) -> () )
    {
        let center = UNUserNotificationCenter.current()
        var nextDate: Date?
        
        center.getPendingNotificationRequests { (requestArray) in
            print("Pending Count: \(requestArray)")
            
            for request in requestArray
            {
                guard let calendarTrigger = request.trigger as? UNCalendarNotificationTrigger,
                    let nextTriggerDate = calendarTrigger.nextTriggerDate() else { return date(nil) }
                
                print(nextTriggerDate)
                
                print("request: \(request.identifier)")
                
                if request.identifier.hasPrefix(idPrefix)
                {
                    if nextDate == nil
                    { nextDate = nextTriggerDate }
                        
                    else if nextTriggerDate < (nextDate ?? Date())
                    { nextDate = nextTriggerDate }
                }
                
            }
            date(nextDate)
        }
    }
    
    func removeAllNotifications()
    {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
//        UserDefaults.standard.set(0, forKey: "NotificationPeriod")
        
        for key in UserDefaults.standard.dictionaryRepresentation().keys {
            if key.hasPrefix("Medicine_id_"){
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
        
        UserDefaults.standard.synchronize()
    }
}























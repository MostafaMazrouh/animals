//
//  HomeViews.swift
//  PetsKFU
//
//  Created by Mostafa on 1/12/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

// MARK: - Custom Cells

class SetUpCell: UITableViewCell
{
    weak var owner: HomeVC?
    func setUp() {}
}

class LanguageCell: SetUpCell
{
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var arButton: UIButton!
    @IBOutlet weak var enButton: UIButton!
    
    
    override func setUp()
    {
        languageLabel.text = NSLocalizedString("Language", comment: "")
        
        if MOLHLanguage.currentAppleLanguage() == "en"
        {
            enButton.setTitleColor(UIColor.blue, for: .normal)
            arButton.setTitleColor(UIColor.gray, for: .normal)
        } else {
            enButton.setTitleColor(UIColor.gray, for: .normal)
            arButton.setTitleColor(UIColor.blue, for: .normal)
        }
        
    }
    
    @IBAction func languageAction(_ sender: FitButton)
    {
        if MOLHLanguage.currentAppleLanguage() == "ar" &&
            sender.tag == 19 { return }
        else if MOLHLanguage.currentAppleLanguage() == "en" &&
            sender.tag == 0 { return }
        
        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
        MOLH.reset()
    }
}

class LogoutCell: SetUpCell
{
    @IBOutlet weak var logoutButton: UIButton!
    
    override func setUp()
    {
        logoutButton.setTitle(NSLocalizedString("Logout", comment: ""), for: .normal)
    }
    
    @IBAction func logoutAction(_ sender: FitButton)
    {
        UserDefaults.standard.set(nil, forKey: "OwnerId")
        UserDefaults.standard.synchronize()
        
        NotifiyVM.shared.removeAllNotifications()
        
        print("logged out..")
        
        owner?.login()
    }
}

class AlertMeCell: SetUpCell
{
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    
    @IBOutlet weak var minuteField: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func setUp()
    {
        minuteField.keyboardType = .numberPad
        
        alertLabel.text = "AlertMe".localized()
        minuteLabel.text = "Minutes".localized()
        
        minuteField.text = "\(NotifiyVM.shared.notifiyMinutes)"
        
        saveButton.setTitle("Save".localized(), for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.backgroundColor = .appGreen
        
        cancelButton.setTitle("Cancel".localized(), for: .normal)
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.backgroundColor = .lightGray
    }
    
    @IBAction func saveAction(_ sender: UIButton)
    {
        guard let text = minuteField.text,
            let minutes = Int(text),
        minutes <= 30 , minutes >= 0
            else {
                Messager.show(message: "MinutesMessage".localized())
                return
        }
        
        NotifiyVM.shared.notifiyMinutes = minutes
        owner?.changeSideViewVisiblity()
    }
    
    
    @IBAction func cancelAction(_ sender: UIButton) {
        owner?.changeSideViewVisiblity()
    }
}

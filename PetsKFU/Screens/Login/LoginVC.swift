//
//  LoginVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/7/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit
import UserNotifications

class LoginVC: UIViewController
{
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var rememberMeLabel: UILabel!
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    @IBOutlet weak var loginButton: UIButton!

    let loginVM = LoginVM()
    let dosesVM = DosesVM()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUp()
    }
    @IBAction func tapAction(_ sender: UITapGestureRecognizer)
    {
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    func setUp()
    {
        loginButton.setTitle(NSLocalizedString("Login", comment: ""), for: .normal)
        rememberMeLabel.text = NSLocalizedString("RememberMe", comment: "")
        
        if let userName = UserDefaults.standard.string(forKey: "username") {
            usernameField.text = userName
        } else {
            usernameField.placeholder = "UserName".localized()
        }
        
        if let password = UserDefaults.standard.string(forKey: "password") {
            passwordField.text = password
        } else {
           passwordField.placeholder = "Password".localized()
        }
        
        backgroundView.backgroundColor = UIColor.appGreen.withAlphaComponent(0.7)
        loginButton.backgroundColor = UIColor.appGreen
        loginButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        loginButton.layer.shadowRadius = 5
        loginButton.layer.shadowOpacity = 0.25
        loginButton.layer.shadowColor = UIColor.white.cgColor
        loginButton.layer.masksToBounds = false
        
        rememberMeSwitch.onTintColor = .appGreen
        rememberMeSwitch.tintColor = .white
    }
    
    // MARK: - Actions
    
    
    
    @IBAction func loginAction(_ sender: UIButton)
    {
        guard let username = usernameField.text, let password = passwordField.text, username.count > 0, password.count > 0  else { return Messager.show(message: "CorrectData".localized()) }
        
        loginVM.login(userName: username, password: password) { [weak self] (str, error) in
            guard let responseStr = str else { return Messager.show(message: "NoAccount") }
            if responseStr.hasPrefix("-1") || responseStr.hasPrefix("-2")
            { return Messager.show(message: "NoAccount".localized()) }
            
            let subStrs = responseStr.split(separator: "{", maxSplits: 2, omittingEmptySubsequences: true)
            
            guard let ownerStr = subStrs.first else { return Messager.show(message: "NoAccount".localized()) }
            
            guard let selfStrong = self else { return }
            
            DispatchQueue.main.async {
            
//            ownerStr = "47"
            UserDefaults.standard.set(ownerStr, forKey: "OwnerId")
            
            if selfStrong.rememberMeSwitch.isOn {
                UserDefaults.standard.set(username, forKey: "username")
                UserDefaults.standard.set(password, forKey: "password")
            } else {
                UserDefaults.standard.set(nil, forKey: "username")
                UserDefaults.standard.set(nil, forKey: "password")
            }
                UserDefaults.standard.synchronize()
                selfStrong.dismiss(animated: true, completion: nil)
                
                selfStrong.dosesVM.getNotificationsOnServer()
            }
        }
    }
    
    
}



















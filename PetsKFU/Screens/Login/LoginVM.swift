//
//  LoginVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/11/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation
import UserNotifications

class LoginVM
{
    private let crModel = CodableRESTModel()
    
    func login(userName: String, password: String, handler: @escaping (String?, Error?)->())
    {
        let url = BASE_URL + "/login"
        
        let dic = ["strUserName":userName,"strPassword":userName]
        
        crModel.postData(url: url, httpBody: dic, removeGarbage: true) { (data, error) in
            
            guard let data = data else { return handler(nil, error) }
            
            if let str = String(data: data, encoding: String.Encoding.utf8)
            {
                handler(str, nil)
            } else {
                 handler(nil, error)
            }
        }
    }
}










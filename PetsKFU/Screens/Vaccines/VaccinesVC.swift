//
//  VaccinesVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/8/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class VaccinesVC: UIViewController
{
    @IBOutlet weak var tableView: UITableView!
    
    let vaccinesVM = VaccinesVM()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Vaccine".localized()
        addBackgroundImage()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
        
        vaccinesVM.getVetDirections { [weak self] (data, error) in
            guard let directionsData = data else
                {return Messager.show(message: "Wrong".localized())}
//            {return Messager.show(message: error?.localizedDescription)}
            
            self?.vaccinesVM.vetDirections = directionsData
            
            DispatchQueue.main.async {
                self?.tableView.reloadData() }
        }
        
    }
}


// MARK: - UITableViewDelegate

extension VaccinesVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return vaccinesVM.vetDirections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DirectionCell", for: indexPath) as! DirectionCell
        
        if indexPath.row < vaccinesVM.vetDirections.count {
            cell.vetDirections = vaccinesVM.vetDirections[indexPath.row]
            cell.setUp()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}



















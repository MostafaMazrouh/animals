//
//  VaccinesVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/17/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class VaccinesVM
{
    private let crModel = CodableRESTModel()
    var vetDirections = [VetDirections]()

    func getVetDirections(handler: @escaping ([VetDirections]?, Error?)->())
    {
        let url = BASE_URL + "/GetVetDirections"
        
        crModel.getData(url: url, receivedType: [VetDirections].self) { (data, error) in
            
            handler(data, error)
        }
    }

}

struct VetDirections: Codable
{
    let Message_id: String?
    let Message_title: String?
    let Message_date_ar: String?
    let Message_date_en: String?
    let Message_content: String?
}





















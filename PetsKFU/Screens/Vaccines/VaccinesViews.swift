//
//  VaccinesViews.swift
//  PetsKFU
//
//  Created by Mostafa on 1/17/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class DirectionCell: UITableViewCell
{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    var vetDirections: VetDirections?
    
    
    func setUp()
    {
        titleLabel.text = vetDirections?.Message_title
        contentLabel.text = vetDirections?.Message_content
        
        if MOLHLanguage.currentAppleLanguage() == "en"
        {
            dateLabel.text = vetDirections?.Message_date_en
        } else {
            dateLabel.text = vetDirections?.Message_date_ar
        }
        
        contentView.cornerRadius = 4
        contentView.borderWidth = 0.2
        contentView.borderColor = .lightGray
    }
}

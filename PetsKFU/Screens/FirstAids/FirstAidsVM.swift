//
//  FirstAidsVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/21/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class FirstAidsVM
{
    private let crModel = CodableRESTModel()
    
    func questionAnswer(userId: String, handler: @escaping ([QuestionAnswer]?, Error?)->())
    {
        let url = BASE_URL + "/GetQuestionsByUser"
        
        let httpBody = ["userID": userId]
        print("httpBody: \(httpBody)")
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            
            guard let qaData = self?.crModel.decode(toType: [QuestionAnswer].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(qaData, nil)
        }
    }
    
    func askQuestion(userId: String, question: String, handler: @escaping (Bool?, Error?)->())
    {
        let url = BASE_URL + "/SubmitQuestion"
        
        let httpBody = ["userID": userId, "question": question]
        print("httpBody: \(httpBody)")
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            let result = String(data: cleanData, encoding: String.Encoding.utf8) as String?
            
            print("result: \(String(describing: result))")
            
            if result == "1" {
                handler(true, nil)
            } else {
                handler(nil, error)
            }
        }
    }
}
















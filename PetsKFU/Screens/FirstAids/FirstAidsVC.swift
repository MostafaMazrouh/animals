//
//  FirstAidsVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/8/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class FirstAidsVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var answeredButton: UIButton!
    @IBOutlet weak var notAnsweredButton: UIButton!
    @IBOutlet weak var askButton: UIButton!
    
    
    @IBOutlet weak var askView: UIView!
    @IBOutlet weak var askSmallView: UIView!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var askTextView: UITextView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelkButton: UIButton!
    
    let vm = FirstAidsVM()
    
    var completeArray = [QuestionAnswer]() {
        didSet {
            answered = [QuestionAnswer]()
            notAnswered = [QuestionAnswer]()
            for qa in completeArray {
                if let answer = qa.QUESTION_ANSWER, answer.count > 0
                {
                    answered.append(qa)
                } else {
                    notAnswered.append(qa)
                }
            }
            
            answered.sort {
                return dateFromString(dateStr: $0.QUESTION_ANS_DATE ?? "") > dateFromString(dateStr: $1.QUESTION_ANS_DATE ?? "")
            }
            
            notAnswered.sort {
                return dateFromString(dateStr: $0.QUESTION_DATE ?? "") > dateFromString(dateStr: $1.QUESTION_DATE ?? "")
            }
        }
    }
    
    func dateFromString(dateStr: String) -> Date
    {
        let dateFormatter = DateFormatter()
        let islamicCalendar = Calendar.init(identifier: Calendar.Identifier.islamicUmmAlQura)
        dateFormatter.calendar = islamicCalendar
        dateFormatter.locale = Locale.init(identifier: "ar")
        dateFormatter.dateFormat = "dd/MM/yy hh:mm:ss a"
        let date = dateFormatter.date(from: dateStr) ?? Date()
        return date
    }
    
    var answered = [QuestionAnswer]()
    var notAnswered = [QuestionAnswer]()
    
    var selectedArray = [QuestionAnswer]() {
        didSet {
            DispatchQueue.main.async { self.tableView.reloadData() }
        }
    }
    
    var showAnswered = true { didSet { DispatchQueue.main.async {self.refresh()} } }
    
    func refresh()
    {
        selectedArray = showAnswered ? answered : notAnswered
        
        if showAnswered {
            answeredButton.setTitleColor(.appOrange, for: .normal)
            notAnsweredButton.setTitleColor(.appGreen, for: .normal)
            selectedArray = answered
        } else {
            answeredButton.setTitleColor(.appGreen, for: .normal)
            notAnsweredButton.setTitleColor(.appOrange, for: .normal)
            selectedArray = notAnswered
        }
    }
        override func viewDidLoad()
        {
            super.viewDidLoad()
            
            self.setUp()
            
            self.getData()
            
        }
    
    func getData()
    {
        guard let userId = UserDefaults.standard.string(forKey:"OwnerId") else
        {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
            return
        }
        
        vm.questionAnswer(userId: userId) { [weak self] (qaArray, error) in
            guard error == nil else
            {return Messager.show(message: "Wrong".localized())}
            
            DispatchQueue.main.async {
                if let qaArray = qaArray {
                    self?.completeArray = qaArray
                    self?.refresh()
                }
            }
        }
    }
    
    func setUp()
    {
        self.title = NSLocalizedString("First aids", comment: "")
        addBackgroundImage()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
        
        answeredButton.setTitle(NSLocalizedString("Answered", comment: ""), for: .normal)
        notAnsweredButton.setTitle(NSLocalizedString("NotAnswered", comment: ""), for: .normal)
        askButton.setTitle(NSLocalizedString("Ask", comment: ""), for: .normal)
        
        showAnswered = true
        
        // Ask view
        askLabel.text = "Ask".localized()
        okButton.setTitle("OK".localized(), for: .normal)
        cancelkButton.setTitle("Cancel".localized(), for: .normal)
        askTextView.textColor = .black
        askLabel.textColor = .black
        okButton.setTitleColor(.appOrange, for: .normal)
        cancelkButton.setTitleColor(.appOrange, for: .normal)
        showAskView(show: false)
    }
    
    func showAskView(show: Bool)
    {
        askTextView.resignFirstResponder()
        
        if show
        {
            askView.isHidden = false
            self.view.bringSubview(toFront: askView)
        }
        else
        {
            askView.isHidden = true
            self.view.sendSubview(toBack: askView)
        }
    }
    
    @IBAction func tapAction(_ sender: UITapGestureRecognizer)
    {
        askTextView.resignFirstResponder()
    }
    
    @IBAction func okAction(_ sender: FitButton)
    {
        showAskView(show: false)
        
        guard let userId = UserDefaults.standard.string(forKey:"OwnerId") else
        { return }
        
        guard let askString = askTextView.text else { return }
        guard askString.count > 10 else {
            Messager.show(message: "ValidQuestion".localized())
            return
        }
        
        self.vm.askQuestion(userId: userId, question: askString, handler: { [weak self] (bool, error) in
            self?.getData()
            if let bool = bool, bool == true {
                Messager.show(message: "QuestionSent".localized())
            }
        })
    }
    
    @IBAction func cancelAction(_ sender: FitButton)
    {
        showAskView(show: false)
    }
    
    @IBAction func answeredAction(_ sender: FitButton)
    {
        showAnswered = (sender.tag == 1) ? true : false
    }
    
    @IBAction func askAction(_ sender: FitButton)
    {
        showAskView(show: true)
    }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
            return selectedArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerCell", for: indexPath) as! QuestionAnswerCell
            
            if indexPath.row < selectedArray.count {
                cell.questionAnswer = selectedArray[indexPath.row]
                cell.setUp()
            }
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
    }
    
    class QuestionAnswerCell: UITableViewCell
    {
        @IBOutlet weak var questionLabel: UILabel!
        @IBOutlet weak var answerLabel: UILabel!
        @IBOutlet weak var dateLabel: UILabel!
        var questionAnswer: QuestionAnswer?
        
        
        func setUp()
        {
            questionLabel.text = questionAnswer?.QUESTION_HEAD
            
            if let answer = questionAnswer?.QUESTION_ANSWER,
                answer.count > 0
            {
                answerLabel.text = questionAnswer?.QUESTION_ANSWER
                 dateLabel.text = questionAnswer?.QUESTION_ANS_DATE
            } else {
                answerLabel.text = NSLocalizedString("NoAnswer", comment: "")
                 dateLabel.text = questionAnswer?.QUESTION_DATE
            }
            questionLabel.textColor = .appOrange
        }
}










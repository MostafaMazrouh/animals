//
//  PersonalProfileVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/9/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class PersonalProfileVC: UIViewController
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var animalsLabel: UILabel!
    @IBOutlet weak var personButton: UIButton!
    @IBOutlet weak var peopleButton: UIButton!
    
    let vm = PersonalProfileVM()
    
    var showAnimals = false {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.refresh()
            } }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
        self.getData()
    }
    
    func setUp()
    {
        self.title = "PersonalProfile".localized()
        
        self.addBackgroundImage()
        userLabel.text = "YourInfo".localized()
        animalsLabel.text = "AnimalsInfo".localized()
        
        personButton.template(imageName: "person", tintColor: .appGreen)
        peopleButton.template(imageName: "people", tintColor: .appGreen)
    }
    
    func getData()
    {
        guard let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId") else
        {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
            return
        }
        
        // Get Personal Profile Data
        vm.getOwnerData(strOwnerID: strOwnerID) { [weak self] (ownerData, error) in
            
            guard let ownerData = ownerData else
            {return Messager.show(message: "Wrong".localized())}
//            {return Messager.show(message: error?.localizedDescription)}
            
            
            
            self?.vm.ownerData = ownerData
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.refresh()
            }
        }
        
        // Get Animals Data
        vm.getOwnerAnimals(strOwnerID: strOwnerID) { [weak self] (ownerAnimal, error) in
            
            guard let ownerAnimals = ownerAnimal else
                {return Messager.show(message: "Wrong".localized())}
            
            self?.vm.ownerAnimalArray = ownerAnimals
        }
    }
    
    @IBAction func switchAction(_ sender: UIBarButtonItem)
    {
        showAnimals = (sender.tag == 19) ? false : true
    }
    
    func refresh()
    {
        if showAnimals {
            personButton.template(imageName: "person", tintColor: .appGreen)
            userLabel.textColor = .appGreen
            
            peopleButton.template(imageName: "people", tintColor: .appOrange)
            animalsLabel.textColor = .appOrange
        } else {
            personButton.template(imageName: "person", tintColor: .appOrange)
            userLabel.textColor = .appOrange
            
            peopleButton.template(imageName: "people", tintColor: .appGreen)
            animalsLabel.textColor = .appGreen
        }
    }
}


// MARK: - UITableViewDelegate

extension PersonalProfileVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return showAnimals ? vm.ownerAnimalArray.count : vm.ownerStructure.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.showAnimals
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerAnimalCell", for: indexPath) as! OwnerAnimalCell
            
            cell.setUp(ownerAnimal: vm.ownerAnimalArray[indexPath.row])
            
            return cell
        }
        
        if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerEditCell", for: indexPath) as! OwnerEditCell
            cell.vc = self
            cell.index = indexPath.row
            guard let item = vm.ownerStructure[indexPath.row] else { return cell }
            cell.setUp(item: item)
            
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerDataCell", for: indexPath) as! OwnerDataCell
        cell.index = indexPath.row
        guard let item = vm.ownerStructure[indexPath.row] else { return cell }
        cell.setUp(item: item)
        
        return cell
    }
    
    func reloadCell(indexPath: IndexPath)
    {
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}





//
//  PersonalProfileVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/13/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class PersonalProfileVM
{
    private let crModel = CodableRESTModel()
    
    var ownerData: OwnerData? { didSet { self.setUpOwnerData() } }
    var ownerStructure = [Int : [String: String?]]()
    
    var ownerAnimalArray = [OwnerAnimal]()
    
    func getOwnerData(strOwnerID: String, handler: @escaping (OwnerData?, Error?)->())
    {
        let url = BASE_URL + "/GetOwnerData"
        let httpBody = ["strOwnerID": strOwnerID]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let ownerData = self?.crModel.decode(toType: OwnerData.self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(ownerData, nil)
        }
    }
    
    func getOwnerAnimals(strOwnerID: String, handler: @escaping ([OwnerAnimal]?, Error?)->())
    {
        let url = BASE_URL + "/GetOwnerAnimals"
        let httpBody = ["strOwnerID": strOwnerID]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let ownerAnimal = self?.crModel.decode(toType: [OwnerAnimal].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(ownerAnimal, nil)
        }
    }
    
    func updatePatientPhone(userId: String, strPhone: String, handler: @escaping (Bool?, Error?)->())
    {
        let url = BASE_URL + "/UpdatePatientPhone"
        
        let httpBody = ["strPatientId": userId, "strPhone": strPhone]
        print("httpBody: \(httpBody)")
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            let result = String(data: cleanData, encoding: String.Encoding.utf8) as String?
            
            print("result: \(String(describing: result))")
            
            if result == "1" {
                handler(true, nil)
            } else {
                handler(nil, error)
            }
        }
    }
}

extension PersonalProfileVM
{
    func setUpOwnerData()
    {
        guard let ownerData = self.ownerData else { return }
        ownerStructure = [Int : [String: String?]]()
        
        //---
        ownerStructure[0] = ["PersonalInfo": ""]
        ownerStructure[1] = ["ArabicName": (ownerData.Owner_name_ar ?? "")]
        ownerStructure[2] = ["EnglishName": (ownerData.Owner_name_en ?? "")]
        ownerStructure[3] = ["Hijribirthdate": (ownerData.Owner_birthdate_ar ?? "")]
        ownerStructure[4] = ["Gregorianbirthdate": (ownerData.Owner_birthdate_en ?? "")]
        ownerStructure[5] = ["Nationality": ownerData.Nationality ?? ""]
        ownerStructure[6] = ["": ""]
        
        //---
        ownerStructure[7] = ["ContactInfo": ""]
        ownerStructure[8] = ["Phonenumber1": (ownerData.Owner_phone1 ?? "")]
        ownerStructure[9] = ["Phonenumber2": (ownerData.Owner_phone2 ?? "")]
        ownerStructure[10] = ["Phonenumber3": (ownerData.Owner_phone3 ?? "")]
        ownerStructure[11] = ["Emailaddress": (ownerData.Owner_email ?? "")]
        ownerStructure[12] = ["Address": (ownerData.Owner_address ?? "")]
        ownerStructure[13] = ["": ""]
        
        //---
        ownerStructure[14] = ["IdentityInfo": ""]
        ownerStructure[15] = ["IDtype": (ownerData.Owner_Identi_Type ?? "")]
        ownerStructure[16] = ["IDnumber": (ownerData.Owner_Identi_No ?? "")]
        ownerStructure[17] = ["IDplace": (ownerData.Owner_Identi_Location ?? "")]
        ownerStructure[18] = ["IDdate": (ownerData.Owner_Identi_Date ?? "")]
        ownerStructure[19] = ["": ""]
    }
}

struct OwnerAnimal: Codable
{
    let PAT_INFO_ID: Int?
    let OWNER_INFO_ID: Int?
    let PAT_TYPE_ID: Int?
    let PAT_TYPE_Name: String?
    let PAT_INFO_FILE_NO: String?
    //    let PAT_INFO_BARCODE: null,
    let PAT_INFO_DOB_AR: String?
    let PAT_INFO_DOB_EN: String?
    //    let PAT_AGE: null
    let PAT_GENDER_ID: Int?
    //    let PAT_GENDER_NAME: null,
    let PAT_NATIONALITY_ID: Int?
    let PAT_BLOOD_TYPE_ID: Int?
    //    let PAT_INFO_NOTES: null
}

struct OwnerData: Codable
{
    let Owner_id: Int?
    let Owner_file_number: String?
    let Owner_barcode: String?
    let Owner_name_ar: String?
    let Owner_name_en: String?
    let Owner_first_name_ar: String?
    let Owner_second_name_ar: String?
    let Owner_third_name_ar: String?
    let Owner_forth_name_ar: String?
    let Owner_first_name_en: String?
    let Owner_second_name_en: String?
    let Owner_third_name_en: String?
    let Owner_forth_name_en: String?
    let Owner_birthdate_ar: String?
    let Owner_birthdate_en: String?
    let Fk_gender_id: Int?
    let Fk_religion_id: Int?
    let Fk_nationality_id: Int?
    let Nationality: String?
    let Owner_phone1: String?
    let Owner_phone2: String?
    let Owner_phone3: String?
    let Owner_email: String?
    let Owner_address: String?
    let Owner_job_title: String?
    let Owner_notes: String?
    let Owner_Identi_Type: String?
    let Owner_Identi_No: String?
    let Owner_Identi_Date: String?
    let Owner_Identi_Location: String?
    let UserName: String?
    let PassWord: String?
}


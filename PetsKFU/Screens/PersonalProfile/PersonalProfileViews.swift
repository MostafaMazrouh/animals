//
//  PersonalProfile.swift
//  PetsKFU
//
//  Created by Mostafa on 1/17/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class OwnerAnimalCell: UITableViewCell
{
    @IBOutlet weak var animalName: UILabel!
    @IBOutlet weak var animalView: UIView!
    
    @IBOutlet weak var fileNumberKey: UILabel!
    @IBOutlet weak var nameKey: UILabel!
    @IBOutlet weak var birthdateKey: UILabel!
    
    @IBOutlet weak var fileNumberValue: UILabel!
    @IBOutlet weak var nameValue: UILabel!
    @IBOutlet weak var birthdateValue: UILabel!
    
    @IBOutlet weak var expandHeight: NSLayoutConstraint!
    
    var ownerAnimal: OwnerAnimal?
    private var doExpand = false
    var indexPath: IndexPath?
    weak var controller: PersonalProfileVC?
    
    func setUp(ownerAnimal: OwnerAnimal?)
    {
        animalView.setRounded()
        
        guard let ownerAnimal = ownerAnimal else { return }
        
        self.animalName.text = ownerAnimal.PAT_TYPE_Name
        
        fileNumberKey.text = "Filenumber".localized()
        nameKey.text = "Arabicname".localized()
        birthdateKey.text = "Hijribirthdate".localized()
        
        fileNumberKey.textColor = .appGreen
        nameKey.textColor = .appGreen
        birthdateKey.textColor = .appGreen
        contentView.cornerRadius = 4
        contentView.borderWidth = 0.2
        contentView.borderColor = .lightGray
        
        fileNumberValue.text = ownerAnimal.PAT_INFO_FILE_NO
        nameValue.text = ownerAnimal.PAT_TYPE_Name
        //        birthdateValue.text = (MOLHLanguage.currentAppleLanguage() == "en") ?  ownerAnimal.PAT_INFO_DOB_EN : ownerAnimal.PAT_INFO_DOB_AR
        birthdateValue.text = ownerAnimal.PAT_INFO_DOB_EN
    }
}

class OwnerDataCell: UITableViewCell
{
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    var index = -1
    var pressed = false
    
    
    func setUp(item: [String: String?])
    {
        guard let key = item.keys.first else {
            keyLabel.text = ""
            valueLabel.text = ""
            return
        }
        keyLabel.text = key.localized()
        valueLabel.text = item[key] ?? ""
        
        if [0, 7, 14].contains(index)
        {
            keyLabel.textColor = UIColor.appOrange
            self.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        } else {
            keyLabel.textColor = UIColor.appGreen
            self.contentView.backgroundColor = UIColor.clear
        }
    }
}


class OwnerEditCell: OwnerDataCell
{
    let vm = PersonalProfileVM()
    weak var vc: PersonalProfileVC?
    
    @IBOutlet weak var editButton: UIButton!
    
    override func setUp(item: [String: String?])
    {
        super.setUp(item: item)
        
        editButton.setTitle("Edit".localized(), for: .normal)
    }
    
    @IBAction func EditAction(_ sender: UIButton)
    {
        let ac = UIAlertController(title: "EditPhone".localized(), message: nil, preferredStyle: .alert)
//        ac.addTextField()
        ac.addTextField { (textField) in
            textField.keyboardType = .numberPad
        }
        
        let okAction = UIAlertAction(title: "OK".localized(), style: .default) { [weak ac] _ in
            if let editString = ac?.textFields![0].text
            {
                print("editString: \(editString)")
                
                guard Int(editString) != nil,
                    editString.count == 10
                    else {
                        Messager.show(message: "ValidPhone".localized())
                        return
                }
                
                
                guard let userId = UserDefaults.standard.string(forKey:"OwnerId") else
                { return }
                
                self.vm.updatePatientPhone(userId: userId, strPhone: editString, handler: { [weak self] (bool, error) in
                    if let bool = bool, bool == true {
                        DispatchQueue.main.async {
                            Messager.show(message: "Updated.")
                            self?.vc?.getData()
                        }
                    } else {
                        DispatchQueue.main.async {
                            Messager.show(message: "UpdatedError.")
                        }
                    }
                })
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { _ in }
        
        ac.addAction(okAction)
        ac.addAction(cancelAction)
        
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return }
        
        var upperVC = rootVC
        if  let navVC = rootVC as? UINavigationController,
            let visibleVC = navVC.visibleViewController
        {
            upperVC = visibleVC
        }
        
        upperVC.present(ac, animated: true, completion: nil)
    }
}

//
//  VisitsModels.swift
//  PetsKFU
//
//  Created by Mostafa on 1/21/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

struct PatientVisit: Codable
{
    let visit_id: String?
    let visit_start_date_ar: String?
    let visit_start_date_en: String?
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy hh:mm:ss"
        let dateAr = dateFormatter.date(from: (visit_start_date_en ?? "")) ?? Date()
        
        dateFormatter.dateFormat = "hh:mm:ss"
        let timeDate = dateFormatter.string(from: dateAr)
        
        return timeDate
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy hh:mm:ss"
        let dateAr = dateFormatter.date(from: (visit_start_date_en ?? "")) ?? Date()
        
        dateFormatter.dateFormat = "dd/mm/yyyy"
        var dateDate = dateFormatter.string(from: dateAr)
        
        dateDate = "\(visit_start_date_ar ?? "") \(dateDate)"
        
        return dateDate
    }
}

struct VisitTransfer: Codable
{
    let Transfer_start_date_ar: String?
    let Transfer_start_date_en: String?
    let dept_from: String?
    let dept_from_en: String?
    let doctor_from: String?
    let doctor_from_en: String?
    let dept_to: String?
    let dept_to_en: String?
    let doctor_to: String?
    let doctor_to_en: String?
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy hh:mm:ss"
        let dateAr = dateFormatter.date(from: (Transfer_start_date_en ?? "")) ?? Date()
        
        dateFormatter.dateFormat = "hh:mm:ss"
        let timeDate = dateFormatter.string(from: dateAr)
        
        return timeDate
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy hh:mm:ss"
        let dateAr = dateFormatter.date(from: (Transfer_start_date_en ?? "")) ?? Date()
        
        dateFormatter.dateFormat = "dd/mm/yyyy"
        var dateDate = dateFormatter.string(from: dateAr)
        
        dateDate = "\(Transfer_start_date_ar ?? "") \(dateDate)"
        
        return dateDate
    }
}





















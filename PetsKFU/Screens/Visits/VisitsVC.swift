//
//  VisitsVC.swift
//  PetsKFU
//
//  Created by Mostafa on 1/9/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import UIKit

class VisitsVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    let vm = VisitsVM()
    
    var patientVisitsArray = [PatientVisit]()
    var visitTransfersArray = [VisitTransfer]()
    
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Visits", comment: "")
        addBackgroundImage()
        
        guard let strOwnerID = UserDefaults.standard.string(forKey:"OwnerId") else
        {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
            return
        }
        
        vm.getPatientVisits(strOwnerID: strOwnerID) { [weak self] (patientVisits, error) in
            guard let patientVisits = patientVisits else { return Messager.show(message: "NoData".localized()) }
            
            DispatchQueue.main.async {
                self?.patientVisitsArray = patientVisits
                self?.tableView.reloadData()
                
                self?.selectRow(indexPath: IndexPath(row: 0, section: 0))
            }
        }
    }
    
    func visitTransfersArray(strVisitId: String)
    {
        vm.getVisitTransfers(strVisitId: strVisitId) { [weak self] (visitTransfers, error) in
            guard let visitTransfers = visitTransfers else {
                self?.visitTransfersArray = [VisitTransfer]()
                return Messager.show(message: "NoData".localized())
            }
            
            DispatchQueue.main.async {
                self?.visitTransfersArray = visitTransfers
                self?.tableView.rectForRow(at: IndexPath(row: 0, section: 1))
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? noInSec0 : noInSec1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var alpha: CGFloat = ((indexPath.row%2)==1) ? 0.15 : 0.25
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VisitCell", for: indexPath) as! VisitCell
            
            if indexPath.row == 0 {
                cell.setUp(visitNum: patientVisitsArray.count)
                cell.contentView.backgroundColor = .white
            } else if indexPath.row == 1 {
                cell.setUp()
                cell.contentView.backgroundColor = .appGreen
            } else if indexPath.row < (patientVisitsArray.count+2) {
                let patientVisit = patientVisitsArray[(indexPath.row-2)]
                cell.setUp(patientVisit: patientVisit)
                if selectedIndexPath == indexPath { alpha = 0.90}
                cell.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(alpha)
            }
            return cell
        }
        
        if indexPath.section == 1 && indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VisitCell", for: indexPath) as! VisitCell
            cell.setUp(transfers: "\(visitTransfersArray.count)")
            cell.contentView.backgroundColor = .white
            return cell
        }
            
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferCell", for: indexPath) as! TransferCell
            
            if indexPath.row == 1 {
                 cell.contentView.backgroundColor = .appGreen;
                cell.setUp()
            }
            else if indexPath.row > 1 &&
                (visitTransfersArray.count > indexPath.row-2)  {
                let visitTransfer = visitTransfersArray[indexPath.row-2]
                cell.setUp(visitTransfer: visitTransfer)
                cell.contentView.backgroundColor = UIColor.lightGray.withAlphaComponent(alpha)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    var noInSec0 = 1
    var noInSec1 = 1
    var selectedTransfers: String?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.selectRow(indexPath: indexPath)
    }
    
    func selectRow(indexPath: IndexPath)
    {
        selectedIndexPath = nil
        if indexPath.row == 1 {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        
        // First display row
        if indexPath.section == 0 && indexPath.row == 0
        {
            guard let cell = tableView.cellForRow(at: indexPath) as? VisitCell else { return }
            if cell.expanded { noInSec0 = 1; cell.expanded = false }
            else { noInSec0 = patientVisitsArray.count+2; cell.expanded = true }
            noInSec1 = 1; selectedTransfers = nil
            tableView.reloadSections([0, 1], with: .automatic)
            
        }
        
        // Normal row in visits
        else if indexPath.section == 0 && indexPath.row > 1
        {
            let patientVisit = patientVisitsArray[indexPath.row - 2]
            self.visitTransfersArray(strVisitId: patientVisit.visit_id ?? "-1")
            selectedTransfers = patientVisit.visit_id
            noInSec1 = 1
            selectedIndexPath = indexPath
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.selectRow(indexPath: IndexPath(row: 0, section: 1))
            }
        }
            
        // Second display row
        else if indexPath.section == 1 && indexPath.row == 0
        {
            noInSec1 = visitTransfersArray.count + 2
            tableView.reloadSections([1], with: .automatic)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
}

class VisitCell: UITableViewCell
{
    @IBOutlet weak var hijriLabel: UILabel!
    @IBOutlet weak var gregorianLabel: UILabel!
    @IBOutlet weak var visitNumberLabel: UILabel!
    var expanded = false
    
    func setUp(transfers: String?)
    {
        visitNumberLabel.text = NSLocalizedString("Transfersno", comment: "")
        hijriLabel.text = transfers ?? ""
        gregorianLabel.text = NSLocalizedString("Display", comment: "")
        gregorianLabel.backgroundColor = .appOrange
    }
    
    func setUp(patientVisit: PatientVisit? = nil, visitNum: Int? = nil)
    {
        if let visitNum = visitNum {
            visitNumberLabel.text = NSLocalizedString("Visitno", comment: "")
            hijriLabel.text = "\(visitNum)"
            gregorianLabel.text = NSLocalizedString("Display", comment: "")
            gregorianLabel.backgroundColor = UIColor.appOrange
            return
        }
        
        gregorianLabel.backgroundColor = UIColor.clear
        guard let patientVisit = patientVisit else {
            visitNumberLabel.text = NSLocalizedString("Visitnumber", comment: "")
            hijriLabel.text = NSLocalizedString("VisitDate", comment: "")
            gregorianLabel.text = NSLocalizedString("VisitTime", comment: "")
            return
        }
        visitNumberLabel.text = patientVisit.visit_id
        
//        hijriLabel.text = patientVisit.visit_start_date_ar
        hijriLabel.text = patientVisit.date
        
//        gregorianLabel.text = patientVisit.visit_start_date_en
        gregorianLabel.text = patientVisit.time
    }
}


class TransferCell: UITableViewCell
{
    @IBOutlet weak var hijriLabel: UILabel!
    @IBOutlet weak var gregorianLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    
    func setUp(visitTransfer: VisitTransfer? = nil)
    {
        guard let visitTransfer = visitTransfer else {
            hijriLabel.text = NSLocalizedString("VisitDate", comment: "")
            gregorianLabel.text = NSLocalizedString("VisitTime", comment: "")
            departmentLabel.text = NSLocalizedString("Todepartment", comment: "")
            doctorLabel.text = NSLocalizedString("Todoctor", comment: "")
            return
        }
//        hijriLabel.text = visitTransfer.Transfer_start_date_ar
        hijriLabel.text = visitTransfer.date
//        gregorianLabel.text = visitTransfer.Transfer_start_date_en
        gregorianLabel.text = visitTransfer.time
        
        departmentLabel.text = (MOLHLanguage.currentAppleLanguage() == "en") ? visitTransfer.dept_to_en : visitTransfer.dept_to
        doctorLabel.text = (MOLHLanguage.currentAppleLanguage() == "en") ? visitTransfer.doctor_to_en : visitTransfer.doctor_to
    }
}

















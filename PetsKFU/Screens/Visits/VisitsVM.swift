//
//  VisitsVM.swift
//  PetsKFU
//
//  Created by Mostafa on 1/21/19.
//  Copyright © 2019 AppsSquare. All rights reserved.
//

import Foundation

class VisitsVM
{
    private let crModel = CodableRESTModel()
    
    func getPatientVisits(strOwnerID: String, handler: @escaping ([PatientVisit]?, Error?)->())
    {
        let url = BASE_URL + "/GetPatientVisits"
        let httpBody = ["strPatientId": strOwnerID]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let patientVisit = self?.crModel.decode(toType: [PatientVisit].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(patientVisit, nil)
        }
    }
    
    func getVisitTransfers(strVisitId: String, handler: @escaping ([VisitTransfer]?, Error?)->())
    {
        let url = BASE_URL + "/GetVisitTransfers"
        let httpBody = ["strVisitId": strVisitId]
        
        crModel.postData(url: url, httpBody: httpBody) { [weak self] (data, error) in
            
            guard error == nil else { return handler(nil, error) }
            guard let mdata = data else { return handler(nil, error) }
            
            guard let cleanData = self?.crModel.removeGarbage(garbageData: mdata) else { return handler(nil, error) }
            
            guard let visitTransfer = self?.crModel.decode(toType: [VisitTransfer].self, from: cleanData) else { return handler(nil, CodableRESTError.decodingError) }
            
            handler(visitTransfer, nil)
        }
    }
}



















